/* eslint-disable no-console */

'use strict';

var express = require('express');
var routes = require('./app/routes/routes');
var mongoose = require('mongoose');
let process = require('process');
require('dotenv').config();
var dbStringGen = require('./lib/dbstringgen');

var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server, {transports: ['websocket', 'polling'], allowEIO3: true});

const expressStatusMonitor = require('express-status-monitor');
app.use(expressStatusMonitor({
    websocket: io,
    port: app.get('port')
}));

var Logger = require(process.cwd() + '/lib/logger');

// this is here for future file upload capability
// var fileFilter = require('./app/utilities/filefilter');
// var multer = require('multer');
// var diskStorage = multer.diskStorage({
//     destination: function(req, file, cb){
//         cb(null, 'uploads');
//     },
//     filename: function(req, file, cb){
//         cb(null, file.originalname);
//     }
// });
// var upload = multer({storage: diskStorage, fileFilter: fileFilter});
var bodyParser = require('body-parser');

var uri = dbStringGen();
var dbName = process.env.NODE_ENV == 'test' ? 'test' : process.env.DB_NAME || 'auctioneer';

mongoose.connect(uri,  {
     family: 4,
     dbName: dbName
//     useFindAndModify: false,
//     useCreateIndex: true
});
mongoose.Promise = require('bluebird');

// At this point I'm not sure why I need to use the adapter
// var { createAdapter } = require('@socket.io/mongo-adapter');
// const { MongoClient } = require("mongodb");
// io.adapter(createAdapter(`mongodb://${dbHost}:${dbPort}/${dbName}`));

app.use('/public', express.static(process.cwd() + '/public'));
app.use('/app', express.static(process.cwd() + '/app'));
app.use('/dist', express.static(process.cwd() + '/dist'));

app.use(bodyParser.json({limit: '50mb'}));
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.urlencoded({limit: '50mb', extended: false, parameterLimit: 50000}));

app.set('queueState', {
    queue: [],
    active: false,
    totalRemaining: null
});

routes(app, io);

var port = process.env.PORT || 80;
server.listen(port,  function () {
    Logger.info('Node.js listening on port ' + port + '...');
});

module.exports = app;
