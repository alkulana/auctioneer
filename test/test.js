var chai = require('chai');
var chaiHttp = require('chai-http');
var expect = chai.expect;
var app;
var fs = require('fs-extra');
var chaiAsPromised = require('chai-as-promised');
var env;
var dbSetup = require(process.cwd() + '/lib/dbsetup');
var Settings = require(process.cwd() + '/app/models/settings');
var Attendees = require(process.cwd() + '/app/models/attendees');
var Items = require(process.cwd() + '/app/models/items');
var Transactions = require(process.cwd() + '/app/models/transactions');
var dbFuncs = require(process.cwd() + '/lib/dbfuncs');
var env;
var Promise = require('bluebird');

chai.use(chaiHttp);
chai.use(chaiAsPromised);

before(function(){
    env = process.env;
    process.env.NODE_ENV = 'test';

    return dbSetup()
        .then(() => {
            return Promise.resolve(app = require(process.cwd() + '/app.js'));
        })
        .catch(e => {
            console.log(e);
        });
});

after(function(){
    return Items.deleteMany().exec()
        .then(() => {
            return Attendees.deleteMany().exec();
        })
        .then(() => {
            return Transactions.deleteMany().exec();
        })
        .then(() => {
            return Settings.deleteMany().exec();
        });
    
});

describe('Setup', function(){
    it('Checks that environment is set to test', function(){
        expect(process.env.NODE_ENV).to.be.equal('test');
    });

    it('Checks that settings exist', function(){
        return Settings.find().exec()
            .then(()=>{
                return expect(Settings.countDocuments().exec()).to.eventually.equal(1);
            });
    });
});