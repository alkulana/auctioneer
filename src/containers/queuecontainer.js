import { connect } from 'react-redux';
import { toggleMenu } from '../actions';
import Queue from '../components/queue';

const mapStateToProps = state => {

    return {
        hideMenu: state.hideMenu
    };
};


const mapDispatchToProps = dispatch => {
    return {
        toggleMenu: ()=>{
            dispatch(toggleMenu());
        }
    };
};

const QueueContainer = connect(mapStateToProps, mapDispatchToProps)(Queue);

export default QueueContainer;