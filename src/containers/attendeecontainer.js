import { connect } from 'react-redux';
import { editAttendee, attendeeSearchValue } from '../actions';
import Attendees from '../components/attendees';

const mapStateToProps = state => {

    return {
        socket: state.socket,
        settings: state.settings,
        attendees: filter(state.attendees.list, state.attendees.searchValue),
        searchValue: state.attendees.searchValue
    };
};

const mapDispatchToProps = dispatch => {
    return {
        editAttendee: data => {
            dispatch(editAttendee(data));
        },
        attendeeSearchValue: searchValue => {
            dispatch(attendeeSearchValue(searchValue));
        }
    };
};


function filter(attendees, searchValue){
    return attendees.filter(att => {
        var re = new RegExp(searchValue, 'gi');
        return re.test(att.number) || re.test(att.name) || re.test(att.phone) || re.test(att.email);
    });
}

const AttendeeContainer = connect(mapStateToProps, mapDispatchToProps)(Attendees);

export default AttendeeContainer;