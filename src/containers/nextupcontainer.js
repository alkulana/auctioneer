import { connect } from 'react-redux';
import {  } from '../actions';
import NextUp from '../components/nextup';

const mapStateToProps = state => {

    return {
        socket: state.socket,
        attendees: state.attendees.list,
        queue: state.queue.queue,
        totalRemaining: state.queue.totalRemaining,
        settings: state.settings
    };
};


const mapDispatchToProps = dispatch => {
    return {};
};

const NextUpContainer = connect(mapStateToProps, mapDispatchToProps)(NextUp);

export default NextUpContainer;