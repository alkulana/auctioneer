import { connect } from 'react-redux';
import Sales from '../components/sales';

const mapStateToProps = state => {

    return {
        socket: state.socket,
        settings: state.settings,
        attendees: state.attendees.list,
        dropDownAttendees: mapAttendees(state.attendees.list),
        items: state.items.list,
        transactions: state.transactions,
        totalRemaining: state.queue.totalRemaining,
        queueActive: state.queue.active
    };
};


const mapDispatchToProps = dispatch => {
    return {};
};

function mapAttendees(attendees){
    return attendees.map((val) => {
        return {
            label: val.number + ' - ' + val.name,
            value: val._id
        };
    });
}


const SalesContainer = connect(mapStateToProps, mapDispatchToProps)(Sales);

export default SalesContainer;
