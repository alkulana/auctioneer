import { connect } from 'react-redux';
import { editItem, itemSearchValue } from '../actions';
import Items from '../components/items';

const mapStateToProps = state => {

    return {
        socket: state.socket,
        settings: state.settings,
        items: filter(state.items.list, state.items.searchValue),
        attendees: state.attendees.list,
        dropDownAttendees: mapAttendees(state.attendees.list)
    };
};

const mapDispatchToProps = dispatch => {
    return {
        editItem: data => {
            dispatch(editItem(data));
        },
        itemSearchValue: searchValue => {
            dispatch(itemSearchValue(searchValue));
        }
    };
};

function mapAttendees(attendees){
    return attendees.map((val) => {
        return {
            label: val.name,
            value: val._id
        };
    });
}

function filter(items, searchValue){
    var hasWinner = function(winner){
        var re = new RegExp(searchValue, 'gi');
        return re.test(winner.name) || re.test(winner.number);
    };

    return items.filter(item => {
        var re = new RegExp(searchValue, 'gi');
        return re.test(item.itemNumber) || re.test(item.itemName) || re.test(item.description) || item.winners.some(hasWinner);
    });
}

const ItemsContainer = connect(mapStateToProps, mapDispatchToProps)(Items);

export default ItemsContainer;
