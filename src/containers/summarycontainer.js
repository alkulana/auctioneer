import { connect } from 'react-redux';
import { } from '../actions';
import Summary from '../components/summary';

const mapStateToProps = state => {

    return {
        summaries: sortedSummaries(state.summaries)
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

function sortedSummaries(summaries){
    summaries.sort((a,b) => {
        return a.number - b.number;
    });
    return summaries;
}


const SummaryContainer = connect(mapStateToProps, mapDispatchToProps)(Summary);

export default SummaryContainer;