import { connect } from 'react-redux';
import {  } from '../actions';
import Admin from '../components/admin';

const mapStateToProps = state => {
    var hasWinners = false;

    for(var i = 0; i < state.items.list.length; i++){
        if(state.items.list[i].winners.length > 0){
            hasWinners = true;
            break;
        }
    }

    return {
        socket: state.socket,
        settings: state.settings,
        attendees: state.attendees.list,
        items: state.items.list,
        transactions: state.transactions,
        hasWinners: hasWinners
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

const AdminContainer = connect(mapStateToProps, mapDispatchToProps)(Admin);

export default AdminContainer;
