import { connect } from 'react-redux';
import { setSocket, updateSettings, updateData, enqueue, toggleMenu } from '../actions';
import AuctionHub from '../components/auctionhub';



const mapStateToProps = state => {
    return {
        socket: state.socket,
        settings: state.settings,
        attendees: state.attendees,
        items: state.items,
        transactions: state.transactions,
        queue: state.queue,
        hideMenu: state.hideMenu
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setSocket: socket => {
            dispatch(setSocket(socket));
        },
        updateSettings: settings => {
            dispatch(updateSettings(settings));
        },
        updateData: data => {
            dispatch(updateData(data));
        },
        enqueue: queue => {
            dispatch(enqueue(queue));
        },
        toggleMenu: () => {
            dispatch(toggleMenu());
        }
    };
};

const WrapperContainer = connect(mapStateToProps, mapDispatchToProps)(AuctionHub);

export default WrapperContainer;
