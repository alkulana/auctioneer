import { connect } from 'react-redux';
import { } from '../actions';
import Overview from '../components/overview';

const mapStateToProps = state => {

    return {
        socket: state.socket,
        settings: state.settings,
        items: state.items.list,
        attendees: state.attendees.list,
        transactions: state.transactions,
        totals: state.totals,
        statusByItem: state.status.statusByItem,
        statusByWinner: state.status.statusByWinner
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};


const OverviewContainer = connect(mapStateToProps, mapDispatchToProps)(Overview);

export default OverviewContainer;