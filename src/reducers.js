import { combineReducers } from 'redux';

const auctioneer = combineReducers({
    socket,
    settings,
    attendees,
    items,
    transactions,
    queue,
    hideMenu,
    totals,
    status,
    summaries
});

function socket(state = null, action){
    switch(action.type){
    case 'SET_SOCKET':
        return action.socket;
    default:
        return state;
    }
}

function settings(state = {}, action){
    switch(action.type){
    case 'UPDATE_SETTINGS':
        return Object.assign({}, action.settings);
    default:
        return state;
    }
}

function attendees(state = {list:[], searchValue: ''}, action){
    switch(action.type){
    case 'UPDATE_DATA':
        return Object.assign({}, state, {list:action.data.attendees});
    case 'ATTENDEE_SEARCH':{
        return Object.assign({}, state, {searchValue: action.searchValue});
    }
    default:
        return state;
    }
}

function items(state = {list:[], searchValue:''} , action){
    switch(action.type){
    case 'UPDATE_DATA':
        return Object.assign({}, state, {list: action.data.items});
    case 'ITEM_SEARCH':
        return Object.assign({}, state, {searchValue: action.searchValue});
    default:
        return state;
    }
}

function transactions(state = [], action){
    switch(action.type){
    case 'UPDATE_DATA':
        return action.data.transactions;
    default:
        return state;
    }
}

function totals(state = {}, action){
    switch(action.type){
    case 'UPDATE_DATA':
        return action.data.totals;
    default:
        return state;
    }
}

function status(state = {statusByItem:{}, statusByWinner: {}, winnersWithItems: []}, action){
    switch(action.type){
    case 'UPDATE_DATA':
        return action.data.status;
    default:
        return state;
    }
}

function summaries(state = [], action){
    switch(action.type){
    case 'UPDATE_DATA':
        return action.data.summaries;
    default:
        return state;
    }
}

function queue(state = {queue:[]}, action){
    switch(action.type){
    case 'UPDATE_QUEUE':
        return action.queue;
    default:
        return state;
    }
}

function hideMenu(state = false, action){
    switch(action.type){
    case 'TOGGLE_MENU':
        return !state;
    default:
        return state;
    }
}


export default auctioneer;
