'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
// import './theme/semantic.less';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import auctioneer from './reducers';
import HubContainer from './containers/hubcontainer';
import 'bootstrap';
import '../public/css/auctioneer.scss'
import '../node_modules/bootstrap-icons/font/bootstrap-icons.css';

const store = createStore(auctioneer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);


class Auction extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <Provider store={store}>
                <div id="auction" style={{width: '100%'}}>
                    <HubContainer />
                </div>
            </Provider>
        );
    }
}


ReactDOM.render(<Auction />, document.getElementById('wrapper'));
