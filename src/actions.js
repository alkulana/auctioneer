export function setSocket(socket){
    return {
        type: 'SET_SOCKET',
        socket: socket
    };
}

export function updateSettings(settings){
    return {
        type: 'UPDATE_SETTINGS',
        settings: settings
    };
}

export function updateData(data){
    return {
        type: 'UPDATE_DATA',
        data: data
    };
}

// This does not appear to be used right now
export function editAttendee(data){
    return {
        type: 'EDIT_ATTENDEE',
        attendeeData: data.attendeeData,
        idx: data.idx
    };
}

// This is not used right now
export function editItem(data){
    return {
        type: 'EDIT_ITEM',
        // attendeeData: data.attendeeData,
        // idx: data.idx
    };
}

export function attendeeSearchValue(searchValue){
    return {
        type: 'ATTENDEE_SEARCH',
        searchValue: searchValue
    };
}

export function itemSearchValue(searchValue){
    return {
        type: 'ITEM_SEARCH',
        searchValue: searchValue
    };
}

export function enqueue(queue){
    return{
        type: 'UPDATE_QUEUE',
        queue: queue
    };
}

export function toggleMenu(){
    return{
        type: 'TOGGLE_MENU'
    };
}
