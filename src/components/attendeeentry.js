import React from 'react';
import { Table, Form, Modal, Popover} from 'react-bootstrap';
import axios from 'axios';
import { formatPhone } from 'Lib/helpers';
const isEqual = require('lodash/isEqual');

export default class Entry extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            ...props,
            deleted: false,
            isValid: true
        };
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {

        if(!isEqual(prevProps, this.props)){
            this.setState({
                ...this.props
            });
        }
    }

    toggleEditable = () => {
        this.setState({editable: !this.state.editable});
    }

    editValue = (e) => {
        var val = {};
        if(e.target.name === 'phone'){
            val[e.target.name] = formatPhone(e.target.value);
        }
        else{
            val[e.target.name] = e.target.value;
        }

        this.setState(val);
    }

    updateAttendee = () => {
        if(this.state.isValid == false){
            this.setState({modalOpen: true});
        }
        else{
            var attendeeData = {
                name: this.state.name,
                number: this.state.number,
                phone: this.state.phone,
                email: this.state.email
            };

            if(this.props._id){
                attendeeData['_id'] = this.props._id;
                this.setState({
                    editable: false
                });
            }
            else{
                this.setState({...this.props});
            }

            axios.post('/api/adddata',
                {
                    dummy: false,
                    clean: false,
                    types: ['attendees'],
                    attendees: attendeeData
                });
        }
    }

    modalClose = () => {
        this.setState({modalOpen: false});
    }

    deleteAttendee = () => {
        this.setState({
            deleted: true,
            editable: false
        });

        axios.post('/api/deleteitems',
            {
                id: this.props._id,
                type: 'attendees'
            })
            .catch(err => {
                console.log(err);
            });
    }

    cancelUpdate = () => {
        if(this.props.new === true){
            this.setState({...this.props});
        }
        else{
            this.setState({
                ...this.props,
                editable: false
            });
        }
    }

    // Currently this only checks against duplicate attendee numbers
    validate = () => {
        this.setState({isValid: this.props.validate(this.state.number)});
    }

    render(){
        if(this.state.editable === false){
            return (
                <tr hidden={this.state.deleted}>
                    <td>{this.state.number}</td>
                    <td>{this.state.name}</td>
                    <td>{this.state.phone}</td>
                    <td>{this.state.email}</td>
                    <td>
                        <div className='icon-group'>
                            <i className="bi-pencil icon-big" style={{color:"blue"}} title='edit' onClick={this.toggleEditable}/>
                            <i className="bi-trash icon-big" style={{color:"red"}} title='delete' onClick={this.deleteAttendee}/>
                        </div>
                    </td>
                </tr>
            );
        }
        else{
            return(
                <tr>
                    <Modal show={this.state.modalOpen} onHide={this.modalClose} size='sm' centered>
                        <Modal.Header closeButton>
                            <Modal.Title>Submission error</Modal.Title>
                        </Modal.Header>
                        <Modal.Body><p>Please check for errors in the information submitted</p></Modal.Body>
                    </Modal>
                    <td>
                        <Form.Control type='number' value={this.state.number} name='number' onChange={this.editValue} placeholder='Number' size='sm' autoFocus={this.props.new} onBlur={this.validate} isInvalid={!this.state.isValid}/>
                        <Form.Control.Feedback type='invalid'>
                            This number is already being used.  Please choose another
                        </Form.Control.Feedback>
                    </td>
                    <td><Form.Control type='text' value={this.state.name} name='name' onChange={this.editValue} placeholder='Name' size='sm'/></td>
                    <td><Form.Control type='text' value={this.state.phone} name='phone' onChange={this.editValue} placeholder='Phone Number' size='sm'/></td>
                    <td><Form.Control type='text' value={this.state.email} name='email' onChange={this.editValue} placeholder='Email' size='sm'/></td>
                    <td>
                        <div className='icon-group'>
                            <i className="bi-check-lg icon-big" style={{color:"green"}} title="update" onClick={this.updateAttendee}/>
                            <i className="bi-x-lg icon-big" style={{color:'orange'}} title='cancel'onClick={this.cancelUpdate}/>
                            {!this.props.new && <i className="bi-trash icon-big" style={{color:"red"}} title='delete' onClick={this.deleteAttendee}/> }
                        </div>
                    </td>
                </tr>
            );
        }
    }
}
