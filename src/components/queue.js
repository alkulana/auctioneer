import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import NextUpContainer from '../containers/nextupcontainer';

export default class Queue extends React.Component{
    constructor(props){
        super(props);
    }

    handleOpen = () =>{
        this.props.toggleMenu();
    }

    handleClose = () =>{
        this.props.toggleMenu();
    }


    render(){
        return(
            <div className='show-queue-container'>
                <Button onClick={this.handleOpen} variant='success'  className={this.props.hideMenu ? "invisible" : "visible mt-5"}>Click to open queue</Button>
                    <Modal show={this.props.hideMenu} animation={false} fullscreen onHide={this.handleClose} className='queue-modal'>
                        <Modal.Header closeButton>
                            <h1 className='serving-header'>Now serving:</h1>
                        </Modal.Header>
                        <span className="portal-wrapper">
                            <NextUpContainer refPage='queue' />
                        </span>
                    </Modal>
            </div>
        );
    }
}
