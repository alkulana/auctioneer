import React from 'react';
import clsx from 'clsx';
const isEqual = require('lodash/isEqual');

export default class Dimmer extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            visible: this.props.visible || false,
            dimmer: true,
            blurred: this.props.blurred || false,
            noclick: this.props.noclick || false
        };
    }

    componentDidUpdate = (prevProps, currentProps) => {
        if(!isEqual(prevProps, this.props)){
            this.setState({
                visible: this.props.visible
            });
        }
    }

    render(){
        const classes = clsx(this.state)
        const h3Class = this.props.visible ? "dimmer-text" : "d-none";
        return(
            <div>
                {this.props.text && <h3 className={h3Class}>{this.props.text}</h3>}
                <div className={classes}>
                {this.props.children}
                </div>
            </div>

        );
    }
}
