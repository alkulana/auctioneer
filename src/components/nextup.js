import React from 'react';
import { Container, Card, Row, Col } from 'react-bootstrap';

export default class NextUp extends React.Component{
    constructor(props){
        super(props);
    }

    componentDidMount(){
        // if(this.props.refPage == 'queue'){
        //     this.setBoxSize(this.props.settings.queueNum);
        //
        //     window.addEventListener('resize', () => {
        //         this.setBoxSize(this.props.settings.queueNum);
        //     });
        // }
    }

    componentDidUpdate(prevProps, prevState){
        if(this.props.refPage == 'queue' && prevProps.queue.length != this.props.queue.length){
            this.setBoxSize(this.props.settings.queueNum);
        }
    }

    // This is no longer used, but still here for reference in case needed
    setBoxSize(queueLength){
        // not perfect, but works pretty well.  Consistent fontsize still.

        let largeBox, largeWidth, largeHeight, smallBox, smallWidth, smallHeight;

        largeBox = document.getElementById('largeBox').getBoundingClientRect();

        largeWidth = largeBox.width;
        largeHeight = largeBox.height;

        smallBox = document.getElementsByClassName('queue-large');
        // max 400x300

        // allows max 3 rows and columns
        let rows = queueLength == 3 ? 2 : Math.ceil(queueLength / 3);
        let cols;
        if (queueLength < 3){
            cols = queueLength;
        }
        else if(queueLength < 5){
            cols = 2;
        }
        else{
            cols = 3;
        }


        smallWidth = (largeWidth / cols) + 15 > 400 ? 400 : (largeWidth / cols);
        smallHeight = smallWidth * .75; // applied in css normally

        // ensures a vertical fit
        if((smallHeight + 25) * rows > largeHeight){
            smallHeight = (largeHeight / rows) - 25 > 300 ? 300 : (largeHeight / rows) - 25;
            smallWidth = smallHeight * 4 / 3;
        }

        for(var i of smallBox){
            i.style.setProperty('--largeWidth', smallWidth);
            i.style.setProperty('--largeHeight', smallHeight);
        }
    }

    render(){
        if(this.props.refPage == 'sales'){
            return (
                <Row className='queue-box sales gx-4'>
                    {this.props.queue.map(att => {
                        return <SmallBox attendee={att} key={att._id} selectWinner={this.props.selectWinner} dequeue={this.props.dequeue}/>;
                    })}
                </Row>
            );
        }
        else if(this.props.refPage == 'queue'){
            return(
                <Container fluid className='queue-container'style={{overflow: 'hidden'}}>
                    <Row md={3} xs={2} className='queue-box queue' id='largeBox'>
                        {this.props.queue.map(att => {
                            // currently allows no actions from this screen, it is read only
                            return<BigBox attendee={att} key={att._id} settings={this.props.settings}/>;
                        })}
                    </Row>
                </Container>
            );
        }
    }
}

class SmallBox extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            iconVisible: false
        };
    }

    showIcon = () => {
        console.log("over")
        this.setState({
            iconVisible: true
        });
    }

    hideIcon = () => {
        this.setState({
            iconVisible: false
        });
    }

    dequeue = e => {
        // this prevents selecting the parent element
        e.stopPropagation();

        this.props.dequeue(this.props.attendee._id);
    }



    render(){

        return (
            <Col>
                <Card className='queue-small' onMouseOver={this.showIcon} onMouseLeave={this.hideIcon} onClick={()=> {
                    this.props.selectWinner(this.props.attendee._id);
                }}>
                    <Card.Body>
                        <div>
                            <i className='bi-x-lg' variant='danger' title='Skip' onClick={this.dequeue}/>
                            <h5>{this.props.attendee.number}</h5>
                            <p>{this.props.attendee.name}</p>
                        </div>
                    </Card.Body>
                </Card>
            </Col>
        );
    }
}

class BigBox extends React.Component{
    constructor(props){
        super(props);
    }

    render(){

        return(
            <Col as={Card} className='queue-large'>
                <Card.Body>
                    <div className='queue-large-content'>
                        <h2>{(this.props.settings.queueDisplay == 'number' || this.props.settings.queueDisplay == 'both' ) && this.props.attendee.number}</h2>
                        {(this.props.settings.queueDisplay == 'name' || this.props.settings.queueDisplay == 'both') && <h6>{this.props.attendee.name}</h6>}
                    </div>
                </Card.Body>
            </Col>
        );
    }
}
