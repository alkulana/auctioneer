import React from 'react';
import { Table, Form } from 'react-bootstrap';
import Select from 'react-select';
const isEqual = require('lodash/isEqual');

export default class Entry extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            winningBid: this.props.winningBid ? Number(this.props.winningBid).toFixed(2) : '',
            winners: this.props.winners,
        };
    }

    componentDidMount = () => {
        const winnerList = this.props.winners.map(each => {
            var win = each;
            win['value'] = each._id;
            win['label'] = each.name;
            return win
        })
        this.setState({
            editWinner: !this.state.editwinner,
            winners: winnerList
        });
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {

        if(!isEqual(prevProps, this.props)){
            this.setState({
                ...this.props,
                winningBid: this.props.winningBid ? Number(this.props.winningBid).toFixed(2) : ''
            });
        }
    }

    handleWinnerChange = (e, v) => {
        var change = {winners: e}

        this.setState({...change}, () => {
            var thinWinnerList = change['winners'].reduce((a, b) => {
                a.push(b.value);
                return a;
            }, [])
            change['winners'] = thinWinnerList;
            change['id'] = this.props._id;
            this.props.addBulkChange(change);
        });
    }

    handleBidChange = e => {
        var change = {};
        change["winningBid"] = e.target.value;

        this.setState({...change}, () => {
            change['id'] = this.props._id;
            this.props.addBulkChange(change);
        });
    }

    formatBlur = ({currentTarget}) => {
        this.setState({
            winningBid: currentTarget.value != '' ? Number(currentTarget.value).toFixed(2) : ''
        });
    }

    render(){
        return(
            <tr>
                <td>{this.props.itemNumber}</td>
                <td>{this.props.itemName}</td>
                <td><Select className="winners" placeholder="Select winner(s)" isMulti options={this.props.attendees} value={this.state.winners} onChange={this.handleWinnerChange}/></td>
                <td>
                    <i className="bi-currency-dollar icon-in-box"/>
                    <Form.Control className='box-with-icon winningBid' value={this.state.winningBid} onChange={this.handleBidChange} onBlur={this.formatBlur}/>
                </td>
            </tr>
        );
    }
}
