import React from 'react';
import clsx from 'clsx';
import { Modal, Button, Form, Row, Col } from 'react-bootstrap';
import Papa from 'papaparse';
import axios from 'axios';

export default class UploadModal extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            status: "preUpload",
            headings: [],
            data: {},
            result: {}
        }
    }

    selectFile = (e) => {
        this.parseCSV(e.target.files[0])
    }

    parseCSV = (file) => {
        Papa.parse(file, {
            complete: results => {
                this.setState({
                    status: "sorting",
                    headings: results.meta.fields,
                    data: results.data
                })
            },
            header: true
        })
    }

    submit = (selectValues, combineName, combinedIdx) => {
        const payload = {
            csvHeadings: this.state.headings,
            csvData: this.state.data,
            uploadType: this.props.uploadType,
            selectValues: selectValues,
            combineName: combineName,
            combinedIdx: combinedIdx
        }
        axios.post('/api/uploadfile', payload)
            .then((resp) => {
                console.log(resp)
                this.setState({
                    status: "result",
                    result: resp
                })
            })
            .catch(err => {
                console.log(err);
            });
        return;
    }

    cleanup = () => {
        this.setState({
            status: "preUpload",
            headings: [],
            data: {},
            result: {}
        },
        this.props.toggleUpload()
        )
    }

    render(){
        let body;
        let footer;
        if(this.state.status == "preUpload"){
            body = (
                <Form.Control type="file" onChange={this.selectFile}/>
            )
            footer = <p className="text-muted">Files must be in csv format with headers</p>
        }
        else if(this.state.status == "sorting"){
            body = <SortData headings={this.state.headings} submit={this.submit} data={this.state.data} uploadType={this.props.uploadType}/>
        }
        else if(this.state.status == "result"){
            body = <Result result={this.state.result} cleanup={this.cleanup}/>
        }

        return(
            <Modal show={this.props.showUpload} onHide={this.cleanup}>
                <Modal.Header closeButton><Modal.Title>Upload file for bulk {this.props.uploadType} entry</Modal.Title></Modal.Header>
                <Modal.Body>
                    {body}
                </Modal.Body>
                <Modal.Footer>
                    {footer}
                </Modal.Footer>
            </Modal>

        );
    }
}

class SortData extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            itemHeadings: ["Number", "Name", "Description"],
            attendeeHeadings: ["Number", "Name", "Phone", "Email"],
            selectValues:{},
            combineName: false,
            combinedIdx: []
        }
    }

    componentDidMount = () =>{
        if(this.props.uploadType == "attendee" && /last[ -_]?(?:name)?/gi.test(this.props.headings.toString()) && /first[ -_]?(?:name)?/gi.test(this.props.headings.toString())){
            this.setState({combineName: true}, this.findMatch)
        }
        else{
            this.findMatch()
        }
    }

    findMatch = () => {
        const passedHeadings = this.props.headings;

        // sets the most likely of the headings that were passed in csv
        // and works some magic to determine if there are separated names that need to be combined

        const headingSet = this.props.uploadType == "item" ? this.state.itemHeadings : this.state.attendeeHeadings;
        let matchedOptions = {};

        headingSet.forEach(head => {
            let reg = new RegExp(head, 'gi');
            
            let match = passedHeadings.find(elem => {
                if(this.state.combineName && head == "Name"){
                    let idxs = [];
                    idxs.push(passedHeadings.findIndex(elem => {
                        return /first[ -_]?(?:name)?/gi.test(elem);
                    }))
                    idxs.push(passedHeadings.findIndex(elem => {
                        return /last[ -_]?(?:name)?/gi.test(elem);
                    }))
                    match = "Combined Name Fields";
                    this.setState({combinedIdx: idxs})
                    return false;
                }
                else{
                    return reg.test(elem);
                }
            })
            if(match){
                matchedOptions[head] = match;
            }
            else if(this.state.combineName){
                matchedOptions[head] = "Combined Name Fields";
            }            
            else if(head == "Number"){
                // if no number field present, generates numbers automatically
                matchedOptions[head] = "Generate"
            }
            else{
                // if no match, selects the first option
                matchedOptions[head] = passedHeadings[0];
            }

        })

        this.setState({selectValues: matchedOptions})
    }

    updateSelect = e =>{
        const key = e.target.id.split("-").pop();
        const update = Object.assign({}, this.state.selectValues);
        update[key] = e.target.value;
        this.setState({selectValues: update});
    }

    submit = () =>{
        this.props.submit(this.state.selectValues, this.state.combineName, this.state.combinedIdx)
    }

    render(){
        const headingSet = this.props.uploadType == "item" ? this.state.itemHeadings : this.state.attendeeHeadings;
        const options = this.props.headings.map((h, idx) => {
            return <option key={h + "-" + idx} value={h}>{h}</option>
        })
        if(this.state.combineName){
            options.unshift(<option key={"combined-extra"} value="Combined Name Fields">Combined Name Fields</option>)
        }
        let sortForm = headingSet.map(h => {
                return (
                <Form.Group as={Row} className="mb-3" key={h}>
                    <Form.Label column>{h}</Form.Label>
                    <Col>
                        <Form.Select id={"select-" + h} value={this.state.selectValues[h]} onChange={this.updateSelect}>
                            {h=="Number" ? [<option key={"generate-num"} value="Generate">Auto Generate Numbers</option>, ...options] : options}
                        </Form.Select>
                    </Col>
                </Form.Group>
            )
        })


        return(
            <div>
                <h5>Please select the correct headings for your data from the csv file</h5>
                <Form>
                    {sortForm}
                    <Button variant="success" onClick={this.submit}>Submit and insert</Button>
                </Form>

            </div>
        )
    }
}

class Result extends React.Component{
    constructor(props){
        super(props);

        this.state = {}
    }

    render(){
        let result;
        if(this.props.result.status == 200){
            result = <p className="text-center">Upload was sucessful!</p>
        }
        else{
            result = (
                <>
                    <p className="text-center">There was an error with the upload.</p>
                    <p className="text-center">Try again or enter data manually.</p>
                </>
            )

        }

        return(
            <div>
                {result}
                <Button variant='success' onClick={this.props.cleanup}>Exit</Button>
            </div>
        )
    }
}
