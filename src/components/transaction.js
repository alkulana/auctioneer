import React from 'react';import { Table, Form, Modal, Button, Container, Col, Row } from 'react-bootstrap';
import axios from 'axios';
import Big from 'big.js';
const isEqual = require('lodash/isEqual');
const isEmpty = require('lodash/isEmpty');
const transform = require('lodash/transform');
import moment from 'moment';

export default class Transaction extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            items: [],
            donation: {
                _id: 'donation',
                itemNumber: null,
                itemName: 'Donation',
                winningBid: null,
                amtPaid: null,
                amtPaying: '0.00'
            },
            paymentType: '',
            currentTransaction: null,
            paymentTypeError: false
        };

        this.paymentOptions = <><option value=''>Select payment type</option>
            <option value='cash'>Cash</option>
            <option value='card'>Card</option>
            <option value='check'>Check</option></>;
    }

    componentDidMount = () => {
        this.setDefaults(this.props.items);
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {

        if(!isEqual(prevProps, this.props) && !this.props.transID){
            this.setDefaults(this.props.items);
        }
        else if(!isEqual(prevProps, this.props) && this.props.transID){
            // sets up for editing transactions
            this.editingDefaults(this.props.items);
        }
    }

    setDefaults = (items) => {
        const defualtItemsValue = items.map(item => {
            let newItem = Object.assign({}, item);
            newItem.amtPaying = Big(item.winningBid).minus(item.amtPaid).toFixed(2);
            newItem.saved = false;
            newItem.winners = item.winners.map(winner => winner._id);

            let alreadyPaidCheck = this.props.transactions.find(trans => {
                if(trans.attendee._id == this.props.winner){
                    var itemList = trans.items.map(it => it.item._id);
                    return itemList.includes(item._id);
                }
                return undefined;
            });

            if(alreadyPaidCheck !== undefined){
                newItem.saved = true;
            }

            return newItem;
        });
        this.setState({
            items: defualtItemsValue,
            donation: {
                _id: 'donation',
                itemNumber: null,
                itemName: 'Donation',
                winningBid: null,
                amtPaid: null,
                amtPaying: '0.00'
            },
            paymentType: '',
            currentTransaction: null
        });
    }

    editingDefaults = items => {
        const thisTransaction = this.props.transactions.find(trans => {
            return trans._id == this.props.transID;
        });

        let itemsInTransaction = thisTransaction.items;

        // an array of ObjectIds of the items in the transaction
        let itemIdsInTransaction = itemsInTransaction.map(item => {
            return item.item._id;
        });

        // props items array without items in the transaction
        let heldItems = items.filter(item => {
            return !itemIdsInTransaction.includes(item._id);
        });

        // sets up items not in transaction and saves them
        heldItems = heldItems.map(item => {
            let newItem = Object.assign({}, item);
            newItem.amtPaying = Big(item.winningBid).minus(item.amtPaid).toFixed(2);
            newItem.saved = true;
            newItem.winners = item.winners.map(winner => winner._id);
            return newItem;
        });

        // sets up items in transaction to show in current transaction
        itemsInTransaction = itemsInTransaction.map(transItem => {

            // this totals the amount already paid for each item excluding the amount from this transaction
            let amtPaid = Big(0);
            this.props.transactions.forEach(trans => {
                let subtotal = trans.items.reduce((acc, item) =>{
                    if(item.item._id == transItem.item._id && this.props.transID != trans._id){
                        return Big(item.amount).plus(acc);
                    }
                    else{
                        return acc;
                    }
                }, Big(0));

                amtPaid = amtPaid.plus(subtotal);
            });

            let newItem = {...transItem.item};
            newItem.amtPaying = Number(transItem.amount).toFixed(2);
            newItem.saved = false;
            newItem.winners = transItem.item.winners.map(winner => winner._id);
            newItem.amtPaid = Number(amtPaid).toFixed(2);

            return newItem;
        });

        let newItems = heldItems.concat(itemsInTransaction);

        let newDonation = Object.assign({}, this.state.donation);
        newDonation.amtPaying = thisTransaction.donationAmt ? Number(thisTransaction.donationAmt).toFixed(2) : '0.00';

        this.setState({
            items: newItems,
            donation: newDonation,
            paymentType: thisTransaction.paymentType,
            currentTransaction: thisTransaction
        });

    }

    updateItems = (e) => {
        const idx = this.state.items.findIndex(item => {
            return item._id === e.target.id;
        });

        var newState = this.state.items;
        newState[idx].amtPaying = e.target.value;

        this.setState({
            items: newState
        });
    }

    formatBlur = ({currentTarget}) => {
        if(currentTarget.id == 'donation'){
            let newState = this.state.donation;
            newState.amtPaying = Number(currentTarget.value).toFixed(2);

            this.setState({
                donation: newState
            });
        }
        else{
            const idx = this.state.items.findIndex(item => {
                return item._id === currentTarget.id;
            });

            let newState = this.state.items;
            newState[idx].amtPaying = Number(currentTarget.value).toFixed(2);
            this.setState({
                items: newState
            });
        }

    }

    toggleSave = id => {
        const idx = this.state.items.findIndex(item => {
            return item._id === id;
        });
        var newState = this.state.items;
        newState[idx].saved = !newState[idx].saved;

        this.setState({
            items: newState
        });
    }

    updateDonation = (e) => {
        var newState = Object.assign({}, this.state.donation, {amtPaying: e.target.value});

        this.setState({
            donation: newState
        });
    }

    totalPaying = () => {
        var itemTotal = this.state.items.filter(item => !item.saved)
            .reduce((acc, item) => {
                if(item.amtPaying === ''){
                    return acc.plus(Big(0));
                }
                else{
                    return acc.plus(Big(item.amtPaying));
                }
            }, Big(0));

        if(this.state.donation.amtPaying === ''){
            return itemTotal.plus(Big(0)).toFixed(2);
        }
        else{
            return itemTotal.plus(Big(this.state.donation.amtPaying)).toFixed(2);
        }
    }

    addSplit = (itemID, attList) => {
        var newItemState = this.state.items;

        const itemIdx = newItemState.findIndex(item => item._id === itemID);

        newItemState[itemIdx].winners = attList;

        this.setState({
            items: newItemState
        },
        this.sendSplit(itemID, attList)
        );
    }

    sendSplit = (itemID, attList) => {
        axios.post('/api/splitcost', {
            itemID: itemID,
            attList: attList
        })
            .then(() => {
            })
            .catch(err => {
                console.log(err);
            });
    }

    submitTransaction = () => {

        if(this.state.paymentType != ''){
            const items = this.state.items.filter(item => {
                return !item.saved;
            });

            // console.log(this.props.winner)
            // console.log(items)
            // console.log(donation)
            // console.log(paymentType)

            axios.post('/api/submittransaction', {
                attendee: this.props.winner,
                items: items,
                donation: this.state.donation.amtPaying,
                paymentType: this.state.paymentType
            })
                .then(() => {
                })
                .catch(err => {
                    console.log(err);
                });
        }
        else{
            this.setState({
                paymentTypeError: true
            });
        }
    }

    submitEdit = () => {
        if(this.state.paymentType != ''){
            const items = this.state.items.filter(item => {
                return !item.saved;
            });

            this.props.submitEdit({
                transID: this.props.transID,
                items: items,
                donation: this.state.donation.amtPaying,
                paymentType: this.state.paymentType
            });
        }
        else{
            this.setState({
                paymentTypeError: true
            });
        }
    }

    updatePaymentType = (e) => {
        this.setState({
            paymentType: e.target.value,
            paymentTypeError: false
        });
    }

    render(){

        return (
            <div>
                <Container className="divided-container">
                    <Row>
                        <Col sm={3}>
                            <LeftSide items={this.state.items.filter(item => item.saved)} toggleSave={this.toggleSave}/>
                        </Col>
                        <Col width={9}>
                            <ReceiptBox donation={this.state.donation} items={this.state.items.filter(item => !item.saved)} currentWinner={this.props.winner}
                                updateItems={this.updateItems} toggleSave={this.toggleSave} totalPaying={this.totalPaying} attendees={this.props.attendees} addSplit={this.addSplit}
                                currentWinnerName={this.props.currentWinnerName} updateDonation={this.updateDonation} submitTransaction={this.submitTransaction} paymentOptions={this.paymentOptions}
                                paymentType={this.state.paymentType} updatePaymentType={this.updatePaymentType} formatBlur={this.formatBlur} transID={this.props.transID} cancelUpdateTransaction={this.props.cancelUpdateTransaction}
                                currentTransaction={this.state.currentTransaction} submitEdit={this.submitEdit} paymentTypeError={this.state.paymentTypeError}/>

                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

// current transaction
class ReceiptBox extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            showSplit: false,
            splitItem: null
        };
    }

    splitPayment = item => {
        this.setState({
            showSplit: true,
            splitItem: item
        });
    }

    addSplit = (itemID, attList) => {
        this.props.addSplit(itemID, attList);
        this.setState({
            showSplit: false,
            splitItem: null
        });
    }

    closeSplit = () => {
        this.setState({
            showSplit: false,
            splitItem: null
        });
    }

    render(){
        let buttons, header;

        if(!this.props.transID){
            buttons = <Button variant='success' size='sm' onClick={this.props.submitTransaction}><i className='bi-cart-fill'> {'   '}Checkout</i></Button>;
            header = <h3>Current transaction for <span className="name-emphasis">{this.props.currentWinnerName}</span></h3>;
        }
        else{
            buttons = (<span><Button variant='sm' size='sm' onClick={this.props.submitEdit}>Update</Button>
                <Button variant='danger' size='sm' onClick={this.props.cancelUpdateTransaction}>Cancel</Button></span>);
            header = <h3>Editing prior transaction for <span className ="name-emphasis">{this.props.currentWinnerName}</span> from {this.props.currentTransaction ? moment(this.props.currentTransaction.timestamp).format('M/D/YY h:m:s a') : ''}</h3>;
        }


        return (
            <div className='receipt-box'>
                {header}
                <Table>
                    <thead>
                        <tr>
                            <th>Item #</th>
                            <th>Name</th>
                            <th>Winning Bid</th>
                            <th>Amount Paid</th>
                            <th>Paying</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.items.map(item => <ItemLine key={item._id} item={item} updateItems={this.props.updateItems} toggleSave={this.props.toggleSave} splitPayment={this.splitPayment} formatBlur={this.props.formatBlur}/>)}
                        <ItemLine item={this.props.donation} updateItems={this.props.updateDonation} formatBlur={this.props.formatBlur} />
                    </tbody>
                    <tfoot>
                        <tr>
                            <th />
                            <th colSpan='3'>
                                <span>Payment type:&nbsp;&nbsp;<Form.Select value={this.props.paymentType} onChange={this.props.updatePaymentType} isInvalid={this.props.paymentTypeError}>{this.props.paymentOptions}</Form.Select></span>
                                <Form.Control.Feedback type='invalid'>Payment type is a required field</Form.Control.Feedback>
                            </th>
                            <th colSpan='1'>Total: $ {this.props.totalPaying()}</th>
                            <th colSpan='1'>{buttons}</th>
                        </tr>
                    </tfoot>
                </Table>
                <SplitBox visible={this.state.showSplit} item={this.state.splitItem} attendees={this.props.attendees} addSplit={this.addSplit} closeSplit={this.closeSplit} currentWinner={this.props.currentWinner} />
            </div>
        );
    }
}

class ItemLine extends React.Component{
    constructor(props){
        super(props);

        this.state = {
        };
    }

    render(){
        return (
            <tr>
                <td>{this.props.item.itemName != 'Donation' && this.props.item.itemNumber}</td>
                <td>{this.props.item.itemName}</td>
                <td>{this.props.item.itemName != 'Donation' && '$ ' + Number(this.props.item.winningBid).toFixed(2)}</td>
                <td>{this.props.item.itemName != 'Donation' && '$ ' + (Number(this.props.item.amtPaid).toFixed(2) || 0)}</td>
                <td><i className='bi-currency-dollar icon-in-sm-box'/><Form.Control name="amtPaying" id={this.props.item._id} value={this.props.item.amtPaying} onChange={this.props.updateItems} onBlur={this.props.formatBlur} style={{width:'15ch'}} className='box-with-icon' type='number'/></td>
                <td>
                    {this.props.item.itemName != 'Donation' &&
                    <span>
                        <Button style={{marginRight:'10px'}} variant={this.props.item.itemName != 'Donation' && this.props.item.winners.length === 1 ? 'outline-warning' : 'warning'} title={this.props.item.itemName != 'Donation' && this.props.item.winners.length > 1 ? 'Currently split' : 'Split payment'} onClick={()=> this.props.splitPayment(this.props.item)} size='sm'><b>1/2</b></Button>
                        <i className='bi-x-lg big-icon' style={{color:'red'}} size='large' title='Remove' onClick={() => this.props.toggleSave(this.props.item._id)}></i>
                    </span>
                    }
                </td>
            </tr>
        );

    }
}

class SplitBox extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            splitList: {}
        };
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        //clears on exit
        if(!this.props.visible && prevProps.visible === true){
            this.setState({
                splitList: {}
            });
        }
        //processes list correctly
        if(this.props.item){
            if(isEmpty(prevState.splitList)){
                this.setState({
                    splitList: this.processToObject(this.props.item.winners)
                });
            }
            else if (prevProps.item && !isEqual(this.props.item.winners, prevProps.item.winners)){
                //only adds in new additions to the winners list (I think)
                var newList = Object.assign(this.state.splitList, this.processToObject(this.props.item.winners));
                this.setState({
                    splitList: newList
                });
            }
        }


    }

    processToObject = winners =>{
        var splitList = {};

        this.props.attendees.map(each => {
            splitList[each._id] = false;
        })

        winners.forEach(winner => {
            splitList[winner] = true;
        });
        return splitList;
    }

    toggleAtt = (e) => {
        var newList = this.state.splitList;

        newList[e.target.value] = !newList[e.target.value];

        this.setState({splitList: newList});
    }

    addSplit = () => {
        var splitList = transform(this.state.splitList, (acc, value, key) =>{
            if(value === true){
                acc.push(key);
                return acc;
            }
        },[]);

        this.props.addSplit(this.props.item._id, splitList);
    }

    render(){
        var attList = this.props.attendees.filter(att => att._id !== this.props.currentWinner).map(att => <Form.Check type='checkbox' key={att._id} label={att.number + ' - ' + att.name} onChange={this.toggleAtt} value={att._id} checked={this.state.splitList[att._id]}/>);

        return(
            <Modal show={this.props.visible} size='sm' onHide={this.props.closeSplit}>
                {this.props.item &&
                <React.Fragment>
                    <Modal.Header>Select attendees to split {this.props.item.itemNumber}-{this.props.item.itemName} with</Modal.Header>
                    <Modal.Body>
                        <Form>
                            {attList}
                        </Form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant='success' onClick={this.addSplit}>Update split</Button>
                        <Button variant='danger' onClick={this.props.closeSplit}>Cancel</Button>
                    </Modal.Footer>
                </React.Fragment>
                }
            </Modal>

        );
    }
}

// saved items
class LeftSide extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        var lines = this.props.items.map(item => <SavedLine key={item._id} item={item} toggleSave={this.props.toggleSave}></SavedLine>);

        return (
            <div>
                <h3>Saved Items</h3>
                <Table>
                    <thead>
                        <tr>
                            <th>Item #</th>
                            <th>Name</th>
                            <th />
                        </tr>
                    </thead>
                    <tbody>
                        {lines}
                    </tbody>
                </Table>
            </div>
        );
    }
}

class SavedLine extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <tr>
                <td>{this.props.item.itemNumber}</td>
                <td>{this.props.item.itemName}</td>
                <td>
                    <i className='bi-arrow-right big-icon' variant='success' title='Add to transaction' onClick={()=> this.props.toggleSave(this.props.item._id)} />
                </td>
            </tr>
        );
    }
}
