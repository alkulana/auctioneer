import React from 'react';
import { Table, Button, Container, Form } from 'react-bootstrap';
import Entry from './attendeeentry';
import UploadModal from './upload';

export default class Attendees extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            showUpload: false
        };
    }

    validate = num => {
        var used = this.props.attendees.map(att => {
            return String(att.number);
        });

        return !used.includes(String(num));
    }

    searchChange = (e) => {
        this.props.attendeeSearchValue(e.target.value);
    }

    toggleUpload = () => {
        this.setState({showUpload: !this.state.showUpload});
    }


    render(){

        var entries = this.props.attendees.map(att => {
            return <Entry {...att} key={att._id} editable={false} new={false} validate={this.validate}/>;
        });

        return (

            <div>
                <Container>
                    <h1>Attendees</h1>
                    <h3>View and edit the auction attendees</h3>
                </Container>
                <div className="divider"></div>
                <Container className="mb-5">
                    <div className='table-heading'>
                        <h3>Add new attendee</h3>
                        <Button variant="primary" onClick={this.toggleUpload}><i className="bi-upload"/>{'   '}Upload File</Button>
                    </div>
                    <Table style={{tableLayout:"fixed"}}>
                        <thead>
                            <tr>
                                <th>Number</th>
                                <th className="w-25">Name</th>
                                <th className="w-25">Phone</th>
                                <th className="w-25">Email</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <Entry editable={true} new={true} name={''} number={''} phone={''} email={''} validate={this.validate}/>
                        </tbody>
                    </Table>
                </Container>
                <Container>
                    <div className='table-heading'>
                        <h3>Current attendees</h3>
                        <Form>
                            <i className="bi-filter icon-in-box"/>
                            <Form.Control placeholder='Filter' title='Filter attendees by number, name, email address, or phone number'
                                onChange={this.searchChange} value={this.props.searchValue} className="box-with-icon"/>
                        </Form>
                    </div>
                    <Table style={{tableLayout:"fixed"}}>
                        <thead>
                            <tr>
                                <th>Number</th>
                                <th className="w-25">Name</th>
                                <th className="w-25">Phone</th>
                                <th className="w-25">Email</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {entries}
                        </tbody>
                    </Table>
                    <UploadModal showUpload={this.state.showUpload} toggleUpload={this.toggleUpload} uploadType={"attendee"}/>
                </Container>
            </div>
        );
    }
}
