import React from 'react';
import { Form, Button, Container, Toast, Row, Col, ToastContainer } from 'react-bootstrap';
import axios from 'axios';
const isEqual = require('lodash/isEqual');


export default class Admin extends React.Component{
    constructor(props){
        super(props);

        this.state = Object.assign(
            {},
            {settings: this.props.settings},
            {
                clearAttendees: false,
                clearItems: false,
                clearWinners: false,
                clearTransactions: false,
                testAttendees: false,
                testItems: false,
                testWinners: false,
                testTransactions: false,
                hideSettingsMessage: true,
                hideAddDataMessage: true,
                addDataMessage: null,
                hideClearDataMessage: true,
                clearDataMessage: null,
                toastMessages: []
            }
        );
    }

    componentDidUpdate = (prevProps, prevState) => {
        if(!isEqual(prevProps.settings, this.props.settings)){
            this.setState({settings: this.props.settings})
        }
    }

    updateSetting = (e) => {
        var val = {settings: this.state.settings};


        if(e.target.type === 'checkbox' && Object.keys(this.state.settings).includes(e.target.name)){
            val.settings[e.target.name] = e.target.checked;
        }
        else if(e.target.type === 'checkbox'){
            val[e.target.name] = e.target.checked;
        }
        else if (Object.keys(this.state.settings).includes(e.target.name)){
            val.settings[e.target.name] = e.target.value;
        }
        else{
            val[e.target.name] = e.target.value;
        }

        if(e.target.name == 'clearWinners' && e.target.checked == true && this.props.transactions.length > 0){
            val['clearTransactions'] = true;
        }
        if(e.target.name == 'clearItems' && e.target.checked == true){
            val['clearWinners'] = true;
        }
        if(e.target.name == 'clearItems' && e.target.checked == true && this.props.transactions.length > 0){
            val['clearTransactions'] = true;
        }
        if(e.target.name == 'clearAttendees' && e.target.checked == true){
            val['clearWinners'] = true;
        }
        if(e.target.name == 'clearAttendees' && e.target.checked == true && this.props.transactions.length > 0){
            val['clearTransactions'] = true;
        }

        this.setState(val);
    }

    submitSettings = () => {
        axios.post('/api/updateSettings', this.state.settings)
            .then(() => {
                let tm = {
                    header: 'Settings updated',
                    message: "Settings have been updated",
                    show: true,
                    key: Date.now()
                }

                this.setState({toastMessages: [...this.state.toastMessages, tm]}, this.clearChecks())
            })
            .catch(err => {
                console.log(err);
            });
    }

    addTestData = () => {
        let list = [];
        if(this.state.testAttendees == true){
            list.push('attendees');
        }
        if(this.state.testItems == true){
            list.push('items');
        }
        if(this.state.testTransactions == true){
            list.push('transactions');
        }
        if(this.state.testWinners == true){
            list.push('winners');
        }

        let addMessage = list.join(', ') + ' have been updated';

        if(addMessage.indexOf(',') > -1){
            if(list.length == 2){
                addMessage = addMessage.replace(',', ' and');
            }
            else if(list.length > 2){
                let idx = addMessage.lastIndexOf(',');
                addMessage = addMessage.slice(0, idx) + ' and' + addMessage.slice(idx);
            }
        }

        addMessage = addMessage.charAt(0).toUpperCase() + addMessage.slice(1);

        axios.post('/api/adddata',
            {
                dummy: true,
                clean: true,
                types: list
            })
            .then(() => {
                let tm = {
                    header: 'Data added',
                    message: addMessage,
                    show: true,
                    key: Date.now()
                }

                this.setState({toastMessages: [...this.state.toastMessages, tm]}, this.clearChecks())
            })
            .catch(err => {
                console.log(err);
            });
    }

    clearData = () => {
        let list = [];
        if(this.state.clearAttendees == true){
            list.push('attendees');
        }
        if(this.state.clearItems == true){
            list.push('items');

        }
        if(this.state.clearTransactions == true){
            list.push('transactions');
        }
        if(this.state.clearWinners == true){
            list.push('winners');
        }

        let clearMessage = list.join(', ') + ' have been removed';

        if(clearMessage.indexOf(',') > -1){
            if(list.length == 2){
                clearMessage = clearMessage.replace(',', ' and');
            }
            else if(list.length > 2){
                let idx = clearMessage.lastIndexOf(',');
                clearMessage = clearMessage.slice(0, idx) + ' and' + clearMessage.slice(idx);
            }
        }

        clearMessage = clearMessage.charAt(0).toUpperCase() + clearMessage.slice(1);

        axios.post('/api/cleardata',
            {
                clean: true,
                types: list
            })
            .then(() => {
                let tm = {
                    header: 'Data removed',
                    message: clearMessage,
                    show: true,
                    key: Date.now()
                }

                this.setState({toastMessages: [...this.state.toastMessages, tm]}, this.clearChecks())
            })
            .catch(err => {
                console.log(err);
            });
    }

    findAndToggle = key => {
        let stateCopy = this.state.toastMessages;
        let idx = stateCopy.findIndex(mess => mess.key == key);
        let mess = stateCopy[idx];
        mess.show = !mess.show;
        stateCopy[idx] = mess;
        this.setState({toastMessages: stateCopy});
    }

    clearChecks = () => {
        this.setState({
            testAttendees: false,
            testItems: false,
            testWinners: false,
            testTransactions: false,
            clearAttendees: false,
            clearItems: false,
            clearWinners: false,
            clearTransactions: false
        })
    }

    render(){
        var tm = this.state.toastMessages
        var toasts = tm.map(message => {
            return (
            <Toast autohide key={message.key} delay={5000} onClose={() => this.findAndToggle(message.key)} show={message.show}>
                <Toast.Header><span className='me-auto'>{message.header}</span></Toast.Header>
                <Toast.Body>{message.message}</Toast.Body>
            </Toast>)
        })

        return(
            <div>
                <Container>
                    <h1>Admin Page</h1>
                    <h6>Please be careful changing these settings as they affect the entire app.</h6>
                </Container>
                <div className='divider' />
                <Container>
                    <ToastContainer id='toast-box' className='position-fixed m-3' position='top-end'>
                        {[toasts]}
                    </ToastContainer>
                    <Form key={this.state.settings._id || 'empty'}>
                        <h3>Queue Settings</h3>
                        <Form.Group>
                            <Form.Group className='inline-form mb-3'>
                                <Form.Label className='me-3'>Number of people in queue: </Form.Label>
                                <Form.Control name="queueNum" as='input' className='thin-input' htmlSize={3} value={this.state.settings.queueNum} onChange={this.updateSetting} />
                            </Form.Group>
                            <Form.Group className='mb-3'>
                                <label>Advance queue by:</label>
                                <Form.Check className='settings-left' type={'checkbox'} name="advanceByPaid" label=" paying" checked={this.state.settings.advanceByPaid} onChange={this.updateSetting} />
                                <Form.Check className='settings-left' type={'checkbox'} name="advanceByClick" label=" clicking number" checked={this.state.settings.advanceByClick} onChange={this.updateSetting} />
                            </Form.Group>
                            <Form.Group className='mb-3'>
                                <label>On queue, display: </label>
                                <Form.Check className='settings-left' type={'radio'} name="queueDisplay" label=" number" checked={this.state.settings.queueDisplay === 'number' && this.state.settings.useNum} value='number' disabled={!this.state.settings.useNum} onChange={this.updateSetting} />
                                <Form.Check className='settings-left' type={'radio'} name="queueDisplay" label=" name" checked={this.state.settings.queueDisplay === 'name'} value='name' onChange={this.updateSetting} />
                                <Form.Check className='settings-left' type={'radio'} name="queueDisplay" label=" both" checked={this.state.settings.queueDisplay === 'both' && this.state.settings.useNum} value='both' disabled={!this.state.settings.useNum} onChange={this.updateSetting} />
                            </Form.Group>
                            <Form.Group className='mb-4'>
                                <label>Queue order:</label>
                                <Form.Check className='settings-left' type={'radio'} name='queueOrder' label=' random' checked={this.state.settings.queueOrder == 'random'} value='random' onChange={this.updateSetting} />
                                <Form.Check className='settings-left' type={'radio'} name='queueOrder' label=' by number' checked={this.state.settings.queueOrder == 'number'} value='number' onChange={this.updateSetting} />
                            </Form.Group>
                            <Form.Group>
                                <Button variant='primary' onClick={this.submitSettings}>Submit</Button>
                            </Form.Group>
                        </Form.Group>
                    </Form>
                </Container>
                {/*<Segment attached>
                    Future upload area
                </Segment>*/}
                <div className='divider'/>
                <Container>
                    <Form>
                        <h3>Database Testing</h3>
                        <Form.Label label='Add test data: ' />
                        <Form.Group>
                            <Form.Check inline type={'checkbox'} name='testAttendees' label=' Add attendees' checked={this.state.testAttendees} onChange={this.updateSetting} />
                            <Form.Check inline type={'checkbox'} name='testItems' className='settings-left' label=' Add items' checked={this.state.testItems} onChange={this.updateSetting} />
                            <Form.Check inline type={'checkbox'} name='testWinners' className='settings-left' label=' Add winners' checked={this.state.testWinners} onChange={this.updateSetting} disabled={!((this.props.attendees.length > 0 || this.state.testAttendees) && (this.props.items.length > 0 || this.state.testItems))} />
                            <Form.Check inline type={'checkbox'} name='testTransactions' className='settings-left' label=' Add purchases' checked={this.state.testTransactions} onChange={this.updateSetting} disabled={!((this.props.attendees.length > 0 || this.state.testAttendees) && (this.props.items.length > 0 || this.state.testItems) && (this.state.testWinners || this.props.hasWinners))} />
                            <Button className='settings-left' variant='primary' onClick={this.addTestData}>Add data</Button>
                        </Form.Group>
                    </Form>
                    <Form>
                        <Form.Label label='Clear data: '/>
                        <Form.Group>
                            <Form.Check inline type={'checkbox'} name='clearAttendees' label='Remove attendees' checked={this.state.clearAttendees} onChange={this.updateSetting} />
                            <Form.Check inline type={'checkbox'} name='clearItems' className='settings-left' label='Remove items' checked={this.state.clearItems} onChange={this.updateSetting} />
                            <Form.Check inline type={'checkbox'} name='clearWinners' className='settings-left' label='Remove winners' checked={this.state.clearWinners} onChange={this.updateSetting} />
                            <Form.Check inline type={'checkbox'} name='clearTransactions' className='settings-left' label='Remove purchases' checked={this.state.clearTransactions} onChange={this.updateSetting} />
                            <Button className='settings-left' variant='warning' onClick={this.clearData}>Clear!</Button>
                        </Form.Group>
                    </Form>
                </Container>
            </div>

        );
    }
}
