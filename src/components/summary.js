import React from 'react';
import axios from 'axios';
import { Container, Table, Button, Row, Col } from 'react-bootstrap';

export default class Summary extends React.Component{
    constructor(props){
        super(props);
    }

    downloadAAR = () => {
        axios.post('/api/getAAR', {}, {responseType: 'blob'})
            .then(response => {
                const filename = response.headers['content-disposition'].match(/filename=\"(.*)\"$/)[1];
                const url = window.URL.createObjectURL(new Blob([response.data], {type: response.headers['content-type']}));
                const link = document.createElement('a');
                link.href = url;
                link.download = filename;
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            })
            .catch(err => {
                console.log(err);
            });
    }

    render(){

        return (

            <div>
                <Container className='summary-header'>
                    <h1>Summary</h1>
                    <h3>Winner summary and reports</h3>
                </Container>
                <div className='divider' />
                <div className='report-box'>
                    <Button className='primary' onClick={this.downloadAAR}>
                        <i className='bi-download' />{'    '}Download after action report
                    </Button>
                </div>
                <div>
                    <Row md={3} xs={1}>
                            {this.props.summaries.map(summary => <WinnerSummary summary={summary} key={summary._id}/>)}
                    </Row>
                </div>
            </div>
        );
    }
}


class WinnerSummary extends React.Component{
    constructor(props){
        super(props);
    }

    render(){

        let itemRows = this.props.summary.itemsWon.map(item => {
            return (
                <tr key={item._id}>
                    <td>{item.itemNumber + ' - ' + item.itemName}</td>
                    <td>{'$ ' + item.itemTotal}</td>
                </tr>
            );
        });

        let status = {
            fullyPaid: 'Fully paid',
            partiallyPaid: 'Partially paid',
            unpaid: 'Unpaid'
        };

        return(

            <Col className='summary-box g-5'>
                <Table>
                    <thead>
                        <tr>
                            <th>{this.props.summary.number + ' - ' + this.props.summary.name}</th>
                            <th className={'status-' + this.props.summary.status}>{status[this.props.summary.status]}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {itemRows}
                        <tr>
                            <td>Donation</td>
                            <td>{'$ ' + this.props.summary.donationAmt}</td>
                        </tr>
                        <tr>
                            <td className='text-end'><b>Total:</b></td>
                            <td><b>{'$ ' + this.props.summary.grandTotal}</b></td>
                        </tr>
                    </tbody>
                </Table>
            </Col>
        );
    }
}
