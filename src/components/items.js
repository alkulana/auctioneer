import React from 'react';
import axios from 'axios';
import { Table, Button, Container, Form, Modal } from 'react-bootstrap';
import Entry from './itemsentry';
import BulkEntry from './bulkitems';
import UploadModal from './upload';


export default class Items extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            modal: false,
            bulkChanges: [],
            showUpload: false
        };
    }

    validate = num => {
        var used = this.props.items.map(item => {
            return String(item.itemNumber);
        });

        return !used.includes(String(num));
    }


    searchChange = e => {
        this.props.itemSearchValue(e.target.value);
    }

    toggleBulkAdd = () => {
        this.setState({
            modal: !this.state.modal,
            bulkChanges: {}
        });

    }

    addBulkChange = change => {
        const id = change.id;
        delete change.id;
        const thisChange = {};
        thisChange[id] = Object.assign({}, this.state.bulkChanges[id], {...change});
        var bulk = Object.assign({}, this.state.bulkChanges, thisChange);
        this.setState({bulkChanges: bulk});
    }

    submitBulkChange = () => {
        axios.post('/api/bulkaddwinners', this.state.bulkChanges)
            .then(() => {
                this.toggleBulkAdd();
            })
            .catch(err => {
                console.log(err);
            });
        return;
    }

    toggleUpload = () => {
        this.setState({showUpload: !this.state.showUpload});
    }

    render(){
        var entries = this.props.items.map(item => {
            return <Entry {...item} key={item._id} editable={false} new={false} validate={this.validate} attendees={this.props.dropDownAttendees}/>;
        });
        var bulkEntries = this.props.items.map(item => {
            const winners = item.winners.map(each => {
                var win = each;
                win['value'] = each._id;
                win['label'] = each.name;
                return win
            })
            return <BulkEntry {...item} key={item._id} attendees={this.props.dropDownAttendees} winners={winners} addBulkChange={this.addBulkChange}/>;
        });

        return (

            <div>
                <Container>
                    <div className='table-heading'>
                        <h1>Items</h1>
                        <Button variant='success' onClick={this.toggleBulkAdd}><i className="bi-trophy"/>{'   '}Bulk Add</Button>
                    </div>
                    <h3>View and edit the auction items</h3>
                </Container>
                <div className='divider'/>
                <Container className='mb-5'>
                    <div className='table-heading'>
                        <h3>Add new item</h3>
                        <Button variant='primary' onClick={this.toggleUpload}><i className='bi-upload'/>{'   '}Upload File</Button>
                    </div>
                    <Table>
                        <thead>
                            <tr>
                                <th>Item #</th>
                                <th className='w-25'>Name</th>
                                <th className='w-50'>Description</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody className="items-table">
                            <Entry editable={true} new={true} itemName={''} itemNumber={''} description={''} validate={this.validate}/>
                        </tbody>
                    </Table>
                </Container>
                <Container>
                    <div className='table-heading'>
                        <h3>Current items</h3>
                        <Form>
                            <i className='bi-filter icon-in-box'/>
                            <Form.Control placeholder='Filter' title='Filter items by number, name, description, or winner'
                                onChange={this.searchChange} value={this.props.searchValue} className='box-with-icon'/>
                        </Form>
                    </div>
                    <Table>
                        <thead>
                            <tr>
                                <th width={1}>Item #</th>
                                <th width={3}>Name</th>
                                <th width={5}>Description</th>
                                <th width={3}>Winners</th>
                                <th width={2}>Winning Bid</th>
                                <th width={2}>
                                    <span>Actions</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody className="items-table">
                            {entries}
                        </tbody>
                    </Table>
                </Container>
                <UploadModal showUpload={this.state.showUpload} toggleUpload={this.toggleUpload} uploadType={"item"}/>
                <Modal show={this.state.modal} onHide={this.toggleBulkAdd} size='xl' scrollable centered>
                    <Modal.Header closeButton><Modal.Title>Add winners to all items</Modal.Title></Modal.Header>
                    <Modal.Body>
                        <Table>
                            <thead>
                                <tr>
                                    <th>Item #</th>
                                    <th>Item Name</th>
                                    <th>Winner(s)</th>
                                    <th>Winning Bid</th>
                                </tr>
                            </thead>
                            <tbody>
                                {bulkEntries}
                            </tbody>
                        </Table>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant='primary' onClick={this.submitBulkChange}>
                            Update <i className='bi-chevron-right' />
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}
