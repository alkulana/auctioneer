import React from 'react';
import {Form, Table, Modal, Dropdown } from 'react-bootstrap';
import TextareaAutosize from 'react-textarea-autosize';
import Select from 'react-select';
import axios from 'axios';
const isEqual = require('lodash/isEqual');

export default class Entry extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            ...props,
            deleted: false,
            isValid: true,
            editWinner: false,
            winningBid: props.winningBid ? Number(props.winningBid).toFixed(2) : ''
        };
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {

        if(!isEqual(prevProps, this.props)){
            this.setState({
                ...this.props,
                winningBid: this.props.winningBid ? Number(this.props.winningBid).toFixed(2) : ''
            });
        }
    }

    toggleEditable = () => {
        this.setState({editable: !this.state.editable});
    }

    editValue = (e) => {
        var val = {};
        val[e.target.name] = e.target.value;

        this.setState(val);
    }

    updateItem = () => {
        if(this.state.isValid == false){
            this.setState({modalOpen: true});
        }
        else{
            var itemData = {
                itemName: this.state.itemName,
                itemNumber: this.state.itemNumber,
                description: this.state.description,
                // winners: this.state.winners
            };

            if(this.props._id){
                itemData['_id'] = this.props._id;
                this.setState({
                    editable: false
                });
            }
            else{
                this.setState({...this.props});
            }

            axios.post('/api/adddata',
                {
                    dummy: false,
                    clean: false,
                    types: ['items'],
                    items: itemData
                });
        }
    }

    modalClose = () => {
        this.setState({modalOpen: false});
    }

    deleteItem = () => {
        this.setState({
            deleted: true,
            editable: false
        });

        axios.post('/api/deleteitems',
            {
                id: this.props._id,
                type: 'items'
            })
            .catch(err => {
                console.log(err);
            });
    }

    cancelUpdate = () => {
        if(this.props.new === true){
            this.setState({...this.props});
        }
        else{
            this.setState({
                ...this.props,
                editable: false,
                winningBid: this.state.winningBid
            });
        }
    }

    validate = () => {
        this.setState({isValid: this.props.validate(this.state.itemNumber)});
    }

    toggleWinner = () => {
        const winnerList = this.props.winners.map(each => {
            var win = each;
            win['value'] = each._id;
            win['label'] = each.name;
            return win
        })
        this.setState({
            editWinner: !this.state.editwinner,
            winnerList: winnerList
        });
    }

    handleWinnerChange = e => {
        // adjusted to address react-select setup
        this.setState({winnerList: e});
    }

    handleBidChange = e => {
        var change = {};
        change["winningBid"] = e.target.value;

        this.setState({...change});
    }

    submitWinnerChange = () => {
        var change = {};
        // returns an array of attendee ids for winners
        var thinWinnerList = this.state.winnerList.reduce((a, b) => {
            a.push(b.value);
            return a;
        }, [])
        change[this.props._id] = {
            winners: thinWinnerList,
            winningBid: this.state.winningBid
        };
        axios.post('/api/updatesinglewinner',
            change
        )
            .catch(err => {
                console.log(err);
            })
            .then(() => {
                this.setState({editWinner: false});
            });
    }

    cancelWinnerUpdate = () => {
        this.setState({
            winners: this.props.winners,
            winningBid: this.props.winningBid ? Number(this.props.winningBid).toFixed(2) : '',
            editWinner: false
        });
    }

    formatBlur = ({currentTarget}) => {
        this.setState({
            winningBid: currentTarget.value != '' ? Number(currentTarget.value).toFixed(2) : ''
        });
    }

    render(){
        const winnerList = this.props.winners ? this.props.winners.reduce((acc, winner, idx) => {
            var text = acc + winner.name;
            text = idx < this.props.winners.length - 1 ? text + ',\n' : text;
            return text;
        }, '') : [];

        if(this.state.editable === false){
            //all read only
            if(this.state.editWinner === false){
                return (
                    <React.Fragment>
                        <tr hidden={this.state.deleted}>
                            <td>{this.state.itemNumber}</td>
                            <td>{this.state.itemName}</td>
                            <td>{this.state.description}</td>
                            <td>{winnerList}</td>
                            <td>{this.state.winningBid ? ('$ ' + this.state.winningBid) : ''}</td>
                            <td>
                                <div className='icon-group'>
                                    <i className="bi-pencil icon-big" style={{color:"blue"}} title='edit' onClick={this.toggleEditable}/>
                                    <i className="bi-trash icon-big" style={{color:"red"}} title='delete' onClick={this.deleteItem}/>
                                    <i className="bi-trophy icon-big" style={{color:"green"}} title='Edit winners' onClick={this.toggleWinner}/>
                                </div>
                            </td>
                        </tr>
                    </React.Fragment>
                );
            }
            //edit winners
            else{
                return (
                    <React.Fragment>
                        <tr hidden={this.state.deleted}>
                            <td>{this.state.itemNumber}</td>
                            <td>{this.state.itemName}</td>
                            <td>{this.state.description}</td>
                            <td><Select className="winnerList" placeholder="Select winner(s)" isMulti options={this.props.attendees} value={this.state.winnerList} onChange={this.handleWinnerChange}/></td>
                            <td>
                                <i className="bi-currency-dollar icon-in-box"/>
                                <Form.Control className='box-with-icon winningBid' value={this.state.winningBid} onChange={this.handleBidChange} onBlur={this.formatBlur}/>
                            </td>
                            <td>
                                <div className='icon-group'>
                                    <i className="bi-check-lg icon-big" style={{color:"green"}} title="update" onClick={this.submitWinnerChange}/>
                                    <i className="bi-x-lg icon-big" style={{color:'orange'}} title='cancel'onClick={this.cancelWinnerUpdate}/>
                                </div>
                            </td>
                        </tr>
                    </React.Fragment>
                );
            }
        }
        //edit item
        else{
            return(
                <tr>
                    <Modal show={this.state.modalOpen} onHide={this.modalClose} size='sm' centered>
                        <Modal.Header closeButton>
                            <Modal.Title>Submission error</Modal.Title>
                        </Modal.Header>
                        <Modal.Body><p>Please check for errors in the information submitted</p></Modal.Body>
                    </Modal>
                    <td>
                        <Form.Control type='number' value={this.state.itemNumber} name='itemNumber' onChange={this.editValue} placeholder='Number' size='sm' autoFocus={this.props.new} onBlur={this.validate} isInvalid={!this.state.isValid}/>
                        <Form.Control.Feedback type='invalid'>
                            This number is already being used.  Please choose another
                        </Form.Control.Feedback>
                    </td>
                    <td><Form.Control type='text' value={this.state.itemName} name='itemName' onChange={this.editValue} placeholder='Name' size='sm'/></td>
                    <td><TextareaAutosize value={this.state.description} name='description' onChange={this.editValue} placeholder='Description' className='form-control form-control-sm'/></td>
                    {!this.props.new && <td>{winnerList}</td>}
                    {!this.props.new && <td>{this.state.winningBid ? ('$ ' + this.state.winningBid) : ''}</td>}
                    <td>
                        <div className='icon-group'>
                            <i className="bi-check-lg icon-big" style={{color:"green"}} title="update" onClick={this.updateItem}/>
                            <i className="bi-x-lg icon-big" style={{color:'orange'}} title='cancel'onClick={this.cancelUpdate}/>
                            {!this.props.new && <i className="bi-trash icon-big" style={{color:"red"}} title='delete'onClick={this.deleteItem}/> }
                        </div>
                    </td>
                </tr>
            );
        }
    }
}
