import React from 'react';
import { Card, Table, Spinner, Container, Row, Col, Stack } from 'react-bootstrap'
import Statistic from './statistic.js'
import Big from 'big.js';
const isEqual = require('lodash/isEqual');

export default class Overview extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            bidTotals: null,
            statusByItem: {
                fullyPaid: [],
                partiallyPaid: [],
                unpaid: []
            },
            statusByWinner: {
                fullyPaid: [],
                partiallyPaid: [],
                unpaid: []
            }
        };
    }

    componentDidMount = () =>{
        let newState = {
            bidTotals: this.getBidTotals()
        };
        // for new mount after inital page load
        if((Object.keys(this.props.statusByItem).length > 0 ||  Object.keys(this.props.statusByWinner).length > 0) && (!isEqual(this.props.statusByWinner, this.state.statusByWinner) || !isEqual(this.props.statusByItem, this.state.statusByItem))){
            newState.statusByItem = this.props.statusByItem;
            newState.statusByWinner = this.props.statusByWinner;
        }
        this.setState(newState);
    }

    componentDidUpdate = (prevProps, prevState) => {
        let newState = {};
        if(!isEqual(this.props.items, prevProps.items) && prevState.bidTotals !== this.getBidTotals()){
            newState.bidTotals = this.getBidTotals();
        }
        if(!isEqual(this.props.statusByItem, prevProps.statusByItem)){
            newState.statusByItem = this.props.statusByItem;
        }
        if(!isEqual(this.props.statusByWinner, prevProps.statusByWinner)){
            newState.statusByWinner = this.props.statusByWinner;
        }

        if(Object.keys(newState).length > 0){
            this.setState(newState);
        }

    }

    getSalesTotals = () => {
        return;
    }

    getBidTotals = () => {
        let total = this.props.items.reduce((acc, item) => {
            return Big(item.winningBid || 0).plus(acc);
        }, Big(0));

        return '$ ' + total.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }

    render(){

        return(
            <Container>
                <Row>
                    <MultiBox isLoading={!(this.props && this.props.attendees)} label='attendees' value={this.props.attendees.length} />
                    <MultiBox isLoading={!(this.props && this.props.items)} label='items' value={this.props.items.length} />
                    <MultiBox isLoading={!(this.state.bidTotals)} label='total bids' value={this.state.bidTotals} width={5} />
                </Row>
                <Row className="mt-3">
                    <SalesBox totals={this.props.totals}/>
                    <Col md={5}>
                        <Stack gap={3}>
                            <MultiBox isLoading={!(this.props && this.props.statusByWinner)} column={false} header='Purchase status by attendee' statsObj={[{label:'Fully paid', value: this.state.statusByWinner.fullyPaid.length, color:'green'}, {label: 'Partially paid', value: this.state.statusByWinner.partiallyPaid.length, color: 'orange'}, {label:'Unpaid', value:this.state.statusByWinner.unpaid.length, color: 'red'}]} />
                            <MultiBox isLoading={!(this.props && this.props.statusByItem)} column={false} header='Purchase status by item' statsObj={[{label:'Fully paid', value: this.state.statusByItem.fullyPaid.length, color:'green'}, {label: 'Partially paid', value: this.state.statusByItem.partiallyPaid.length, color: 'orange'}, {label:'Unpaid', value:this.state.statusByItem.unpaid.length, color: 'red'}]} />
                        </Stack>
                    </Col>
                </Row>
            </Container>
        );
    }
}

class MultiBox extends React.Component{
    constructor(props){
        super(props);
    }

    makeStats(obj){
        return (obj.map((stat, idx) => {
            return(
                <Statistic className='text-center' color={stat.color} value={stat.value} label={stat.label} key={idx} />
            );
        })
        );
    }

    render(){

        var stats;

        if(this.props.statsObj){
            stats = this.makeStats(this.props.statsObj);
        }
        else{
            stats = this.makeStats([{label: this.props.label, value: this.props.value}]);
        }

        let card = (
            <Card className='stat-box'>
                {this.props.header && <Card.Header className='text-left stats-header'>{this.props.header} </Card.Header>}
                <Card.Body className='text-center'>
                    {this.props.isLoading && <Spinner>Loading {this.props.loadingCat || this.props.label}</Spinner>}
                    {stats}
                </Card.Body>
            </Card>
        );

        if(this.props.column === false){
            return (
                <React.Fragment>
                    {card}
                </React.Fragment>
            );

        }
        else{
            return(
                <Col md={this.props.width ? this.props.width : null}>
                    {card}
                </Col>
            );
        }
    }
}

class SalesBox extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            totals: {
                byItem: {
                    cash: Big(0),
                    card: Big(0),
                    check: Big(0),
                    itemTotal: Big(0)
                },
                byDonation: {
                    cash: Big(0),
                    card: Big(0),
                    check: Big(0),
                    donationTotal: Big(0)
                },
                cashTotal: Big(0),
                checkTotal: Big(0),
                cardTotal: Big(0),
                cardAfterFeeTotal: Big(0),
                grandTotal: Big(0),
                afterFeeTotal: Big(0)
            }
        };
    }

    componentDidMount = () => {
        // for new mount after inital page load
        if(Object.keys(this.props.totals).length > 0 && !isEqual(this.props.totals, this.state.totals)){
            this.setState({totals: this.props.totals});
        }
    }

    componentDidUpdate = (prevProps, prevState) => {
        if(!isEqual(this.props.totals, prevProps.totals)){
            this.setState({
                totals: this.props.totals
            });
        }
    }


    render(){
        return(
            <Col xs={this.props.width ? this.props.width : null}>
                <Card className='stat-box'>
                    <Card.Header className='text-left stats-header'>
                        Sales totals
                    </Card.Header>
                    <Card.Body>
                        {this.props.isLoading && <Spinner>Loading {this.props.loadingCat || this.props.label}</Spinner>}
                        <Table>
                            <thead>
                                <tr>
                                    <th>Payment type</th>
                                    <th>Items</th>
                                    <th>Donations</th>
                                    <th>Totals</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr style={{color:'green'}}>
                                    <td>Cash</td>
                                    <td>{'$ ' + Big(this.state.totals.byItem.cash).toFixed(2)}</td>
                                    <td>{'$ ' + Big(this.state.totals.byDonation.cash).toFixed(2)}</td>
                                    <td>{'$ ' + Big(this.state.totals.cashTotal).toFixed(2)}</td>
                                </tr>
                                <tr style={{color:'orange'}}>
                                    <td>Check</td>
                                    <td>{'$ ' + Big(this.state.totals.byItem.check).toFixed(2)}</td>
                                    <td>{'$ ' + Big(this.state.totals.byDonation.check).toFixed(2)}</td>
                                    <td>{'$ ' + Big(this.state.totals.checkTotal).toFixed(2)}</td>
                                </tr>
                                <tr style={{color:'blue'}}>
                                    <td>Card</td>
                                    <td>{'$ ' + Big(this.state.totals.byItem.card).toFixed(2)}</td>
                                    <td>{'$ ' + Big(this.state.totals.byDonation.card).toFixed(2)}</td>
                                    <td>{'$ ' + Big(this.state.totals.cardTotal).toFixed(2) + ' / $ ' + Big(this.state.totals.cardAfterFeeTotal).toFixed(2) + '*'}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr style={{fontWeight: 'bold'}}>
                                    <th>Totals</th>
                                    <th>{'$ ' + Big(this.state.totals.byItem.itemTotal).toFixed(2)}</th>
                                    <th>{'$ ' + Big(this.state.totals.byDonation.donationTotal).toFixed(2)}</th>
                                    <th>{'$ ' + Big(this.state.totals.grandTotal).toFixed(2) + ' / $ ' + Big(this.state.totals.afterFeeTotal).toFixed(2) + '*'}</th>
                                </tr>
                            </tfoot>
                        </Table>
                    </Card.Body>
                    <Card.Body className='text-right stats-footer'>
                        * estimated total after Square fees
                    </Card.Body>
                </Card>
            </Col>
        );
    }
}
