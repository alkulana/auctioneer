import React from 'react';
import clsx from 'clsx';

export default class Statistic extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        const className = clsx(["statistic", this.props.size, this.props.className])
        return(
            <div className={className} style={{color: this.props.color}}>
                <div className="statistic-value">{this.props.value}</div>
                {this.props.label && <div className="statistic-label">{this.props.label}</div>}
            </div>

        );
    }
}
