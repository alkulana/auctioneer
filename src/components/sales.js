import React from 'react';import { Container, Form, Button, CardGroup, Row, Col } from 'react-bootstrap';
import Statistic from './statistic';
import Select from 'react-select';
import Transaction from './transaction';
import CompletedTransaction from './completedtransaction';
import NextUp from '../containers/nextupcontainer';
import Big from 'big.js';
const isEqual = require('lodash/isEqual');
import axios from 'axios';

export default class Sales extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            selectedWinner: null,
            currentWinner: null,
            completedTransactions: [],
            transID: null
        };
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {

        if(!isEqual(prevProps.items, this.props.items) && this.state.currentWinner){
            const sortedItems = this.sortItemsByPaid(this.state.currentWinner);

            this.setState({
                paidItems: sortedItems.paidItems,
                unpaidItems: sortedItems.unpaidItems
            });
        }

        if(!isEqual(prevProps.transactions, this.props.transactions) && this.state.currentWinner){
            const completedTransactions = this.props.transactions.filter(trans => {
                return trans.attendee._id == this.state.currentWinner;
            });

            this.setState({
                completedTransactions: completedTransactions
            });
        }
    }

    handleWinnerChange = (e) => {
        this.setState({
            selectedWinner: e
        },
        this.handleSubmitWinnerChange);
    }

    selectWinner = id => {
        this.setState({
            selectedWinner: id
        },
        this.handleSubmitWinnerChange);
    }

    handleSubmitWinnerChange = () => {
        const winnerID = this.state.selectedWinner.hasOwnProperty('value') ? this.state.selectedWinner.value : this.state.selectedWinner;
        const sortedItems = this.sortItemsByPaid(winnerID);
        const winnerName = this.props.attendees.find(att => att._id == winnerID).name;
        const completedTransactions = this.props.transactions.filter(trans => {
            return trans.attendee._id == winnerID;
        });
        //needs to check to see if transaction is open, ask for confirmation
        this.setState({
            currentWinner: this.state.selectedWinner.hasOwnProperty('value') ? this.state.selectedWinner.value : this.state.selectedWinner,
            selectedWinner: null,
            paidItems: sortedItems.paidItems,
            unpaidItems: sortedItems.unpaidItems,
            currentWinnerName: winnerName,
            completedTransactions: completedTransactions,
            transID: null
        });
    }

    sortItemsByPaid = (winnerID) => {
        // finds items that winner is connected to (not just the ones they have paid for)
        let winnerItems = this.props.items.filter(item => {
            return item.winners.some(winner => {
                return winner._id == winnerID;
            });
        });

        // assembles array of just item ids from above
        const winnerItemIds = winnerItems.map(item => {
            return item._id;
        });

        let paidItems = [];
        let unpaidItems = [];

        // assemles array of transactions connected to above items
        let connectedTransactions = this.props.transactions.filter(trans => {
            return trans.items.some(item => {
                return winnerItemIds.includes(item.item._id);
            });
        });

        // sums how much is paid on items
        winnerItems.forEach(item => {
            let itemId = item._id;
            let amtPaid = connectedTransactions.reduce((acc, trans) => {
                let itemObj = trans.items.find(it => {
                    return it.item._id == itemId;
                });

                if(itemObj && itemObj.amount){

                    return Big(itemObj.amount).plus(acc);
                }
                else{
                    return acc;
                }
            }, Big(0));
            item.amtPaid = amtPaid;

            // paid off already
            if(amtPaid >= item.winningBid){
                paidItems.push(item);
            }
            // not yet paid off
            else{
                unpaidItems.push(item);
            }
        });

        return {
            paidItems: paidItems,
            unpaidItems: unpaidItems
        };
    }

    deleteTransaction = transID => {
        axios.post('/api/deletetransaction', {
            transID: transID
        })
            .then(() => {
                console.log('Transaction deleted');
            })
            .catch(err => {
                console.log(err);
            });
    }

    //will need function to clear this as well
    editTransaction = transID => {
        this.setState({
            transID: transID
        });
    }

    submitEdit = update => {

        this.setState({
            transID: null
        },
        () => {
            axios.post('/api/edittransaction', update)
                .then(() => {
                    console.log('transaction has been edited');
                })
                .catch(err => {
                    console.log(err);
                }
                );
        }
        );
    }

    cancelUpdateTransaction = () => {
        this.setState({
            transID: null
        });
    }

    enqueueWinners = () => {
        axios.post('/api/queueactions', {
            action: 'enqueue'
        })
            .then(() => console.log('winners queued'))
            .catch(err => {
                console.log(err);
            });
    }

    cancelQueue = () => {
        axios.post('/api/queueactions', {
            action: 'cancel'
        });
    }

    dequeue = attID => {
        axios.post('/api/queueactions', {
            action: 'dequeue',
            id: attID
        })
            .then(() => {
                this.setState({
                    currentWinner: null,
                    selectedWinner: null,
                    paidItems: null,
                    unpaidItems: null,
                    currentWinnerName: null,
                    completedTransactions: [],
                    transID: null
                });

                console.log('dequeue successful');
            })
            .catch(err => {
                console.log(err);
            });
    }

    test = (e, v) =>{
        console.log(e)
        console.log(v)
    }

    render(){
        let queueButtons;

        if(!this.props.queueActive){
            queueButtons = <Button variant='success' onClick={this.enqueueWinners}>Queue winners</Button>;
        }
        else if(this.props.queueActive && this.props.totalRemaining > 0){
            queueButtons = <Button variant='warning' onClick={this.cancelQueue}>Stop queue</Button>;
        }


        return (

            <div>
                <Container>
                    <div className='sales-header flex'>
                        <h1>Sales</h1>
                        <div className="up-next" hidden={!this.props.queueActive}>
                            <NextUp selectWinner={this.selectWinner} dequeue={this.dequeue} refPage='sales'/>
                        </div>
                        <div className='queue-actions'>
                            {this.props.queueActive && (<Statistic size='small' value={this.props.totalRemaining} label="Winners remaining" className='me-5'/>)}
                            {queueButtons}
                        </div>
                    </div>
                </Container>
                <div className='divider' />
                <Container>
                    <div className="transaction-report">
                        <div className='flex'>
                            <h2>Checkout</h2>
                            <div className="select-box">
                                <Form>
                                    <Form.Group as={Row}>
                                        <Col xs="auto">
                                            <Select id='salesWinnerSelect' options={this.props.dropDownAttendees} placeholder="Select winner" value={this.state.selectedWinner} onChange={this.handleWinnerChange}/>
                                        </Col>
                                        {/*<Col xs="auto">
                                            <Button variant='primary' onClick={this.handleSubmitWinnerChange} onKeyPress={this.handleSubmitWinnerChange}>Go!</Button>
                                        </Col>*/}
                                    </Form.Group>
                                </Form>
                            </div>
                        </div>
                        <div className='divider' />
                        {this.state.currentWinner && <Transaction winner={this.state.currentWinner} transactions={this.props.transactions} transID={this.state.transID} attendees={this.props.attendees} items={this.state.unpaidItems} currentWinnerName={this.state.currentWinnerName} cancelUpdateTransaction={this.cancelUpdateTransaction} submitEdit={this.submitEdit}/>}
                    </div>
                </Container>
                <Container className="pt-5">
                    <div>
                        <h3>Previous transactions</h3>
                        <div className='divider' />
                        <div style={{minHeight:'1rem'}}>
                            <Row xs={4} className='g-4'>
                                {this.state.completedTransactions.map(trans => {
                                    return <CompletedTransaction transaction={trans} key={trans._id} deleteTransaction={this.deleteTransaction} editTransaction={this.editTransaction} currentTrans={this.state.transID}/>;
                                })}
                            </Row>
                        </div>
                    </div>
                </Container>
            </div>
        );
    }
}
