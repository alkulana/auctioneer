import React from 'react';
import Dimmer from './dimmer';
import { Table, Card, Modal, Button, Col } from 'react-bootstrap';
import moment from 'moment';

export default class CompletedTransaction extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            trashButton: true,
            editButton: true,
            active: false,
            showConfirm: false
        };
    }

    toggleColor = ({currentTarget}) => {
        let newState = {};
        newState[currentTarget.id] = !this.state[currentTarget.id];

        this.setState(newState);
    }

    confirmDelete = () => {
        this.setState({
            showConfirm: true
        });
    }

    cancelDelete = () => {
        this.setState({
            showConfirm: false
        });
    }

    deleteTransaction = () => {
        this.setState({
            showConfirm: false
        },
        this.props.deleteTransaction(this.props.transaction._id));
    }

    editTransaction = () => {
        this.props.editTransaction(this.props.transaction._id);
    }

    render(){

        const rows = this.props.transaction.items.map(item => {
            return(
                <tr className='table-no-bottom-line' key={item._id}>
                    <td className='table-no-bottom-line'>{item.item.itemNumber + ' - ' + item.item.itemName}</td>
                    <td className='table-no-bottom-line text-right w-45'>{'$ ' + Number(item.amount).toFixed(2)}</td>
                </tr>
            );
        });


        if(this.props.transaction.donationAmt && this.props.transaction.donationAmt !=0){
            rows.push(<tr className='table-no-bottom-line' key='donation'>
                <td className='table-no-bottom-line'>Donation</td>
                <td className='table-no-bottom-line text-right'>{'$ ' + Number(this.props.transaction.donationAmt).toFixed(2)}</td>
            </tr>
            );
        }

        return (
            <Col>
                <Card className='receipt-tape' bg='light'>
                    <Dimmer blurred noclick visible={this.props.currentTrans == this.props.transaction._id} text="Currently editing this transaction">
                        <Card.Header className='text-right'>
                            <span>
                                <i className='bi-pencil pe-3' id='editButton' style={{color:'blue'}} disabled={this.state.editButton} onMouseOver={this.toggleColor} onMouseOut={this.toggleColor} onClick={this.editTransaction}/>
                                <i className='bi-trash' id='trashButton' style={{color:'red'}} disabled={this.state.trashButton} onMouseOver={this.toggleColor} onMouseOut={this.toggleColor} onClick={this.confirmDelete}/>
                            </span>
                        </Card.Header>
                        <Card.Body>
                            <h6 className='receipt-mono'>Sales Receipt</h6>
                            <Table className='receipt-mono'>
                                <tbody>
                                    {rows}
                                    <tr className='table-no-bottom-line'>
                                        <td className='table-no-bottom-line text-right'>Total: </td>
                                        <td className='table-no-bottom-line table-dotted-top-line text-right'>{'$ ' + this.props.transaction.paymentTotal}</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </Card.Body>
                        <Card.Footer>
                            <p className='receipt-mono'>{moment(this.props.transaction.timestamp).format('M/D/YY h:mm:ss a')}</p>
                        </Card.Footer>
                        <Modal show={this.state.showConfirm} className='confirm-buttons'>
                            <Modal.Header>Delete transaction?</Modal.Header>
                            <Modal.Body>Are you sure you want to delete this transaction?  This cannot be undone.</Modal.Body>
                            <Modal.Footer className='text-right'>
                                <Button onClick={this.cancelDelete}>Cancel</Button>
                                <Button onClick={this.deleteTransaction}>OK</Button>
                            </Modal.Footer>
                        </Modal>
                    </Dimmer>
                </Card>
            </Col>
        );
    }
}
