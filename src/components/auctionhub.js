import React from 'react';
const io = require('socket.io-client');
const socket = io({'transports': ['websocket', 'polling']});
import { Tab, Tabs, Container, TabContainer } from 'react-bootstrap'
import Heading from './header';
import AdminContainer from '../containers/admincontainer';
import AttendeeContainer from '../containers/attendeecontainer';
import ItemsContainer from '../containers/itemscontainer';
import SalesContainer from '../containers/salescontainer';
import QueueContainer from '../containers/queuecontainer';
import OverviewContainer from '../containers/overviewcontainer';
import SummaryContainer from '../containers/summarycontainer';

export default  class AuctionHub extends React.Component{
    constructor(props){
        super(props);

        socket.on('settings', settings => {
            this.props.updateSettings(settings);
        });

        socket.on('data', data => {
            console.log(data)
            this.props.updateData(data);
        });

        socket.on('queue', queue => {
            this.props.enqueue(queue);
        });

    }

    componentDidMount = () => {
        this.props.setSocket(socket);
    }

    render(){

        return (
            <Container className='pb-5'>
                {!this.props.hideMenu && <Heading hideClass/>}
                <Tabs defaultActiveKey="Overview" id="main-menu" className={this.props.hideMenu ? "mb-3 d-none" : "mb-3 d-all"}>
                    <Tab eventKey="Overview" title="Overview">
                        <OverviewContainer />
                    </Tab>
                    <Tab eventKey="Attendees" title="Attendees">
                        <AttendeeContainer />
                    </Tab>
                    <Tab eventKey="Items" title="Items">
                        <ItemsContainer />
                    </Tab>
                    <Tab eventKey="Sales" title="Sales">
                        <SalesContainer />
                    </Tab>
                    <Tab eventKey="Queue" title="Queue">
                        <QueueContainer />
                    </Tab>
                    <Tab eventKey="Summary" title="Summary">
                        <SummaryContainer />
                    </Tab>
                    <Tab eventKey="Admin" title="Admin">
                        <AdminContainer />
                    </Tab>
                </Tabs>
            </Container>
        );
    }
}
