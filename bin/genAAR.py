import sys
from openpyxl import load_workbook
from openpyxl.styles import NamedStyle, Border, Side, Alignment
from openpyxl.chart import PieChart, Reference
from openpyxl.drawing.image import Image
from copy import copy
import json

# sys.argv[1] is the path to the report template
# sys.argv[2] is the path to the finalized report
# json data for the report and the auction date are passed in through stdin


def main():
    template_path = sys.argv[1]
    save_name = sys.argv[2]
    stdin = sys.stdin.readlines()
    data = stdin[0]
    auction_date = stdin[1]
    data = json.loads(data)
    
    template = load_workbook(filename = template_path)
    template = generate_receipts(template, data['summaries'], auction_date)
    template = generate_item_bids(template, data['items'], auction_date)
    template = generate_overview(template, data, auction_date)
    
    template.save(save_name)
    print(save_name)

    sys.exit(0)
    
def generate_overview(template, data, auction_date):
    ws = template['Overview']
    
    ws['D3'] = auction_date
    
    ws['C5'] = len(data['items'])
    ws['E5'] = data['itemsSold']
    ws['B8'] = len(data['attendees'])
    ws['D8'] = len(data['status']['statusByWinner']['fullyPaid']) + len(data['status']['statusByWinner']['partiallyPaid']) + len(data['status']['statusByWinner']['unpaid'])
    ws['I8'] = len(data['donationOnly'])
    
    ws['C15'] = float(data['totals']['byItem']['cash'])
    ws['D15'] = float(data['totals']['byDonation']['cash'])
    ws['C16'] = float(data['totals']['byItem']['check'])
    ws['D16'] = float(data['totals']['byDonation']['check'])
    ws['C17'] = float(data['totals']['byItem']['card'])
    ws['D17'] = float(data['totals']['byDonation']['card'])
    
    
    pie = PieChart()
    labels = Reference(ws, min_col=2, min_row=15, max_row=17)
    data = Reference(ws, min_col=5, min_row=15, max_row=17)
    pie.add_data(data, titles_from_data=False)
    pie.set_categories(labels)
    pie.title = "Income by type"
    pie.height = 5.5
    pie.width = 7.5
    ws.add_chart(pie, "G13")
    
    return template

def generate_item_bids(template, items, auction_date):
    ws = template['Item Bids']
    
    bd = Side(style='thin', color="000000")
    
    item_num_col = NamedStyle(name="Item_num_col")
    item_num_col.font = copy(ws['B6'].font)
    item_num_col.fill = copy(ws['B6'].fill)
    item_num_col.border = Border(left=bd, top=bd, right=bd, bottom=bd)
    item_num_col.alignment = copy(ws['B6'].alignment)
    item_num_col.number_format = copy(ws['B6'].number_format)
    
    item_name_col = NamedStyle(name="Item_name_col")
    item_name_col.font = copy(ws['C6'].font)
    item_name_col.fill = copy(ws['C6'].fill)
    item_name_col.border = Border(left=bd, top=bd, right=bd, bottom=bd)
    item_name_col.alignment = copy(ws['C6'].alignment)
    item_name_col.number_format = copy(ws['C6'].number_format)
    
    item_bid_col = NamedStyle(name="Item_bid_col")
    item_bid_col.font = copy(ws['D6'].font)
    item_bid_col.fill = copy(ws['D6'].fill)
    item_bid_col.border = Border(left=bd, top=bd, right=bd, bottom=bd)
    item_bid_col.alignment = copy(ws['D6'].alignment)
    item_bid_col.number_format = copy(ws['D6'].number_format)
    
    ws['B3'] = auction_date
    
    starting_row = 6

    idx = 0
    
    for item in items:
        current_row = starting_row + idx
        
        if current_row != starting_row and idx != len(items) - 1:
            ws.insert_rows(current_row + 1)
            
        item_num_cell = ws.cell(column=2, row=current_row, value=item.get('itemNumber', ''))
        item_num_cell.style = item_num_col
        item_name_cell = ws.cell(column=3, row=current_row, value=item.get('itemName', ''))
        item_name_cell.style = item_name_col
        item_bid_cell = ws.cell(column=4, row=current_row, value=item.get('winningBid', ''))
        item_bid_cell.style = item_bid_col
        ws.row_dimensions[current_row].height = 21
        
        idx = idx + 1
    

    total_cell = 'D' + str(starting_row + idx)
    
    ws[total_cell] = "=SUM(D" + str(starting_row) + ":D" + str(starting_row + idx - 1) + ")"
    
    ws.row_dimensions[starting_row + idx].height = 21
    
    return template

def generate_receipts(template, summaries, auction_date):
    # later, may add image to receipt, not now though
    
    receipt_template = template['Receipt_template']

    for attendee in summaries:
        row = 10;
        
        receipt = template.copy_worksheet(receipt_template)
        
        receipt.title = attendee['name']
        receipt['B2'] = auction_date
        receipt['C4'] = attendee['name']
        receipt['C5'] = attendee['phone']
        receipt['C6'] = attendee['email']
        
        for item in attendee['itemsWon']:
            receipt.cell(column=2, row=row, value=item['itemNumber'])
            receipt.cell(column=3, row=row, value=item['itemName'])
            receipt.cell(column=4, row=row, value=float(item['itemTotal']))
            row = row + 1
        
        if 'donationAmt' in attendee:
            receipt.cell(column=3, row=row + 1, value='Donation:')
            receipt.cell(column=4, row=row + 1, value=float(attendee['donationAmt']))

        receipt.sheet_view.showGridLines = False

    template.remove(template['Receipt_template'])
    
    return template    
        

if __name__ == "__main__":
    main()