FROM node:21-alpine as builder
# FROM alpine as builder
COPY ./ ./
ARG DYNO=true
RUN npm install
RUN npm run build



# FROM node:17-alpine
# RUN apk add py3-pip
FROM alpine
RUN apk add nodejs npm python3 py3-pip
WORKDIR /app
COPY --from=builder dist ./dist/
COPY ./app.js ./Pipfile ./README.MD ./package.json ./
COPY ./sample/ ./sample/
COPY ./reports/ ./reports/
COPY ./public/ ./public/
COPY ./lib/ ./lib/
COPY ./bin/ ./bin/
COPY ./app/ ./app/

ENV NODE_ENV=production
ENV DB_HOST=host.docker.internal
ENV DB_PORT=27017
ENV DB_NAME=auctioneer
RUN pip install pipenv --break-system-packages
RUN npm install

EXPOSE 80

CMD node lib/dbsetup.js ; npm start

# -env/e or --env-file will override these env variables
