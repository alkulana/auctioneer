'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Setting = new Schema({
    type: {type: String, default: 'default'},
    useNum: {type: Boolean, default: true},
    startNum: {type: Number, default: 100},
    queueNum: {type: Number, default: 3},
    advanceByPaid: {type: Boolean, default: true},
    advanceByClick: {type: Boolean, default: false},
    queueDisplay: {type: String, default: 'number'},
    queueOrder: {type: String, default: 'random'}
});

var settingModel = mongoose.model('Setting', Setting);

module.exports = settingModel;