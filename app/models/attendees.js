'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Attendee = new Schema({
    number: {type: Number, unique: true},
    name: String,
    phone: String,
    email: String
});

var attendeeModel = mongoose.model('Attendee', Attendee);

module.exports = attendeeModel;