'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Big = require('big.js');

var Transaction = new Schema({
    timestamp: {type: Date, default: Date.now},
    attendee: {type: Schema.Types.ObjectId, ref: 'Attendee'},
    items: [{item: {type: Schema.Types.ObjectId, ref: 'Item'}, amount: Number}],
    donationAmt: {type: Number, default: 0},
    paymentType: String
});

Transaction.virtual('paymentTotal').get(function(){

    let itemsTotal = this.items.reduce((acc, item) => {
        return Big(item.amount).plus(acc);
    }, Big(0));
    
    let donation = this.donationAmt ? Big(this.donationAmt) : Big(0);
    
    return itemsTotal.plus(donation).toFixed(2);
});

Transaction.virtual('totalAfterFees').get(function(){

    let itemsTotal = this.items.reduce((acc, item) => {
        return Big(item.amount).plus(acc);
    }, Big(0));
    
    let donation = this.donationAmt ? Big(this.donationAmt) : Big(0);
    
    let total = itemsTotal.plus(donation);
    
    if(this.paymentType == 'card'){
        total = total.times(.9725);
    }
    
    return total.round(2, 1);
});

var transactionModel = mongoose.model('Transaction', Transaction);

module.exports = transactionModel;