'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Item = new Schema({
    itemNumber: {type: Number, unique: true},
    itemName: String,
    description: String,
    photo: Buffer,
    winningBid: Number,
    winners: [{type: Schema.Types.ObjectId, ref: 'Attendee'}
        // paid: Boolean,
        // amountPaid: Number,
        // transID: {type: Schema.Types.ObjectId, ref: 'Transaction'}
    ],
    transactions: [{type: Schema.Types.ObjectId, ref: 'Transaction'}]
});

Item.virtual('totals').get(function(){
    let paid = 0;
    this.winners.forEach(function(w){
        paid += w.amountPaid;
    });
    
    let remaining = this.winningBid - paid;
    return {paid: paid, remaining: remaining};
});

var itemModel = mongoose.model('Item', Item);

module.exports = itemModel;