'use strict';

var dbFuncs = require(process.cwd() + '/lib/dbfuncs');
var dummyAttendees = require(process.cwd() + '/sample/dummypeople.json');
var dummyItems = require(process.cwd() + '/sample/dummyitems.json');
var queueFuncs = require(process.cwd() + '/lib/queuefuncs');
var reportGen = require(process.cwd() + '/lib/reportgen');
var Promise = require('bluebird');
var path = require('path');
var fs = require('fs-extra');
var Logger = require(process.cwd() + '/lib/logger');

module.exports = function(app, io){

    app.route('/')
        .get(function(req, res){
            res.sendFile(process.cwd() + '/public/index.html');
        });


    app.route('/api/adddata')
        .post(function(req, res){
            if(req.body.dummy === true){
                req.body.attendees = dummyAttendees;
                req.body.items = dummyItems;
            }
            dbFuncs.addData(req.body)
                .then(data => {
                    io.emit('data', data);
                    res.sendStatus(200);
                })
                .catch(err => {
                    Logger.error(err);
                });
        });

    app.route('/api/updatesettings')
        .post(function(req, res){
            dbFuncs.updateSettings(req.body)
                .then(settings => {
                    io.emit('settings', settings);
                    res.sendStatus(200);
                })
                .catch(err => {
                    Logger.error(err);
                });
        });

    app.route('/api/cleardata')
        .post(function(req, res) {
            dbFuncs.dataClean(req.body)
                .then(data => {
                    io.emit('data', data);
                    res.sendStatus(200);
                })
                .catch(err => {
                    Logger.error(err);
                });
        });

    app.route('/api/deleteitems')
        .post(function(req, res){
            dbFuncs.deleteData(req.body)
                .then((data) => {
                    io.emit('data', data);
                    res.sendStatus(200);
                })
                .catch(err => {
                    Logger.error(err);
                });
        });

    app.route('/api/bulkaddwinners')
        .post(function(req, res){
            dbFuncs.bulkAddWinners(req.body)
                .then(data => {
                    queueFuncs.updateQueue(app.get('queueState'))
                        .then(queue => {
                            io.emit('data', data);
                            io.emit('queue', queue);
                            res.sendStatus(200);
                        });
                })
                .catch(err => {
                    Logger.error(err);
                });
        });

    app.route('/api/updatesinglewinner')
        .post(function(req, res){
            dbFuncs.updateSingleWinner(req.body)
                .then(data => {
                    queueFuncs.updateQueue(app.get('queueState'))
                        .then(queue => {
                            io.emit('data', data);
                            io.emit('queue', queue);
                            res.sendStatus(200);
                        });
                })
                .catch(err => {
                    Logger.error(err);
                });
        });

    app.route('/api/splitcost')
        .post(function(req, res){
            dbFuncs.splitCost(req.body)
                .then(data => {
                    queueFuncs.updateQueue(app.get('queueState'))
                        .then(queue => {
                            io.emit('data', data);
                            io.emit('queue', queue);
                            res.sendStatus(200);
                        });

                })
                .catch(err => {
                    Logger.error(err);
                });
        });

    app.route('/api/submittransaction')
        .post(function(req, res){
            dbFuncs.addTransactions(req.body)
                .then(data => {
                    queueFuncs.dequeue(app.get('queueState'), req.body.attendee)
                        .then(queue => {
                            app.set('queueState', queue);
                            io.emit('data', data);
                            io.emit('queue', queue);
                            res.sendStatus(200);
                        });
                })
                .catch(err => {
                    Logger.error(err);
                });
        });

    app.route('/api/deletetransaction')
        .post(function(req, res){
            dbFuncs.deleteTransaction(req.body)
                .then(data => {
                    queueFuncs.updateQueue(app.get('queueState'))
                        .then(queue => {
                            io.emit('data', data);
                            io.emit('queue', queue);
                            res.sendStatus(200);
                        });

                })
                .catch(err => {
                    Logger.error(err);
                });
        });

    app.route('/api/edittransaction')
        .post(function(req, res){
            dbFuncs.editTransaction(req.body)
                .then(data => {
                    queueFuncs.updateQueue(app.get('queueState'))
                        .then(queue => {
                            io.emit('data', data);
                            io.emit('queue', queue);
                            res.sendStatus(200);
                        });

                })
                .catch(err => {
                    Logger.error(err);
                });
        });

    app.route('/api/queueactions')
        .post(function(req, res){
            if(req.body.action == 'dequeue'){
                queueFuncs.dequeue(app.get('queueState'), req.body.id)
                    .then(queue => {
                        app.set('queueState', queue);
                        io.emit('queue', queue);
                        res.sendStatus(200);
                    })
                    .catch(err => {
                        Logger.error(err);
                    });
            }
            else if(req.body.action == 'enqueue'){
                queueFuncs.enqueue(app.get('queueState'))
                    .then(queue => {
                        app.set('queueState', queue);
                        io.emit('queue', queue);
                        res.sendStatus(200);
                    })
                    .catch(err => {
                        Logger.error(err);
                    });
            }
            else if(req.body.action == 'cancel'){
                let newQueueState = app.get('queueState');
                newQueueState.active = false;
                newQueueState.queue = [];
                app.set('queueState', newQueueState);
                io.emit('queue', newQueueState);
                res.sendStatus(200);
            }
        });

    app.route('/api/getAAR')
        .post(function(req, res){
            reportGen.generateAAR()
                .then(resp => {
                    let filename = path.basename(resp);
                    res.on('finish', () => {
                        fs.remove(resp)
                            .catch(err => {
                                Logger.error(err);
                            });
                    });
                    res.download(resp, filename, {headers: {filename: filename}});
                })
                .catch(err => {
                    Logger.error(err);
                });
        });

        app.route('/api/uploadfile')
            .post(function(req, res){
                dbFuncs.handleUpload(req.body)
                    .then(data => {
                        io.emit('data', data);
                        res.sendStatus(200);
                    })
                    .catch(err => {
                        Logger.error(err);
                        res.sendStatus(500);
                    });
            });

    // ***************************************
    // socket.io "routes"

    //currently may cause an error on the first time run ever or after update, a page refresh will fix
    io.on('connection', socket => {
        dbFuncs.getSettings()
            .then(settings => {
                return Promise.resolve(socket.emit('settings', settings));
            })
            .then(() => {
                return dbFuncs.updateData();
            })
            .then(data => {
                return Promise.resolve(socket.emit('data', data));
            })
            .then(() => {
                let queueState = app.get('queueState');
                socket.emit('queue', queueState);
            })
            .catch(err => {
                Logger.error(err);
            });
    });
};
