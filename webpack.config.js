const path = require('path');
require('dotenv').config();
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// const FontelloPlugin = require('fontello-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin'); // This helps make fontello work
// see https://github.com/gabiseabra/fontello-webpack-plugin/issues/18

module.exports = {
    mode: process.env.NODE_ENV || 'development',

    devtool: 'eval-source-map',

    entry: {
        auctioneer: './src/auctioneer.js'
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    },
    resolve: {
        alias: {
            Lib: path.join(__dirname, 'lib')
        }
    },

    plugins:[
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css'
        })
    ],
    module: {
        rules:[
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.css$/,
                use: [  MiniCssExtractPlugin.loader, 'css-loader' ]
            },
            {
                test: /\.less$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            url: false,
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'less-loader',
                        options: {
                            lessOptions: {
                                math: "always",
                                sourceMap: true
                            }
                        }
                    }
                ]
            },
            {
                  test: /\.(scss)$/,
                  use: [{
                    // inject CSS to page
                    loader: MiniCssExtractPlugin.loader,
                  }, {
                    // translates CSS into CommonJS modules
                    loader: 'css-loader'
                  }, {
                    // Run postcss actions
                    loader: 'postcss-loader',
                    options: {
                      // `postcssOptions` is needed for postcss 8.x;
                      // if you use postcss 7.x skip the key
                      postcssOptions: {
                        // postcss plugins, can be exported to postcss.config.js
                        plugins: function () {
                          return [
                            require('autoprefixer')
                          ];
                        }
                      }
                    }
                  }, {
                    // compiles Sass to CSS
                    loader: 'sass-loader'
                  }]
            },
            {
               test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
               type: 'asset',   // <-- Assets module - asset
               parser: {
                 dataUrlCondition: {
                   maxSize: 8 * 1024 // 8kb
                 }
               },
               generator: {  //If emitting file, the file path is
                 filename: 'fonts/[hash][ext][query]'
               }
            },
            {
               test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
               type: 'asset/resource',  //<-- Assets module - asset/resource
               generator: {
                 filename: 'images/[hash][ext][query]'
               }
             }
        ],
    }
};
