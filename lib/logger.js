'use strict';

var winston = require('winston');
const { format } = winston;
const { combine, colorize, timestamp, printf, errors } = format;
var fs = require('fs-extra');
var moment = require('moment');

fs.ensureDir(process.cwd() + '/log');
// const myFormat = printf(info => {
//     return `[${moment().format('ddd MM-DD-YYYY HH:mm:ss').toString()}] -- ${info.level}: ${info.message}`;
// });
// const humanReadableFormatter = printf(({ level, message, ...metadata }) =>{
//     const filename = getFileNameAndLineNumber();
//     return `[${level}] [${filename}] ${message} ${JSON.stringify(metadata)}`;
// })
const myFormat = printf(({ level, message, timestamp, stack }) => {
                if (stack) {
                    // print log trace
                    return `${timestamp} ${level}: ${message} - ${stack}`;
                }
                return `${timestamp} ${level}: ${message}`;
            });

const logger = winston.createLogger({
    format: combine(
        errors({stack:true}),
        // colorize(),
        timestamp(),
        myFormat
    ),
    exitOnError: false,
    transports: [
        // - Write to all logs with level `info` and below to `combined.log`
        // - Write all logs error (and below) to `error.log`.
        //
        new winston.transports.File({
            filename: process.cwd() + '/log/error.log',
            level: 'error',
            tailable: true,
            maxFiles: 10,
            maxSize: 2000000,
            // format: combine(
            //     errors({stack:true}),
            //     // colorize(),
            //     timestamp(),
            //     myFormat
            // ),
            // colorize: { all: true }
        }),
        new winston.transports.File({
            filename: process.cwd() + '/log/combined.log',
            level: 'info',
            tailable: true,
            maxFiles: 10,
            maxSize: 2000000,
            // format: myFormat,
            colorize: { all: true },
        }),
        // new winston.transports.Loggly({ })
    ]
});


//
// If we're not in production then log to the `console`:

if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        format: myFormat
    }));
}

// /**
//  * /**
//  * Use CallSite to extract filename and number, for more info read: https://v8.dev/docs/stack-trace-api#customizing-stack-traces
//  * @param numberOfLinesToFetch - optional, when we want more than one line back from the stacktrace
//  * @returns {string|null} filename and line number separated by a colon, if numberOfLinesToFetch > 1 we'll return a string
//  * that represents multiple CallSites (representing the latest calls in the stacktrace)
//  *
//  */
// const getFileNameAndLineNumber = function getFileNameAndLineNumber (numberOfLinesToFetch = 1) {
//     const oldStackTrace = Error.prepareStackTrace;
//
//     const boilerplateLines = line => line &&
//         line.getFileName() &&
//         (line.getFileName().indexOf('auctioneer') &&
//         (line.getFileName().indexOf('/node_modules/') < 0));
//
//     try {
//         // eslint-disable-next-line handle-callback-err
//         Error.prepareStackTrace = (err, structuredStackTrace) => structuredStackTrace;
//         Error.captureStackTrace(this);
//         // we need to "peel" the first CallSites (frames) in order to get to the caller we're looking for
//         // in our case we're removing frames that come from logger module or from winston
//         const callSites = this.stack.filter(boilerplateLines);
//         if (callSites.length === 0) {
//             // bail gracefully: even though we shouldn't get here, we don't want to crash for a log print!
//             return null;
//         }
//         const results = [];
//         for (let i = 0; i < numberOfLinesToFetch; i++) {
//             const callSite = callSites[i];
//             let fileName = callSite.getFileName();
//             fileName = fileName.includes(require('path').resolve('.')) ? fileName.substring(require('path').resolve('.').length + 1) : fileName;
//             results.push(fileName + ':' + callSite.getLineNumber());
//         }
//         return results.join('\n');
//     } finally {
//         Error.prepareStackTrace = oldStackTrace;
//     }
// };



// const logger = winston.createLogger({
//   transports: [
//       new winston.transports.Console({
//             level: 'info',
//             handleExceptions: true,
//             humanReadableUnhandledException: true,
//             json: false,
//             colorize: { all: true },
//             stderrLevels: ['error', 'alert', 'critical', 'bizAlert'],
//             format: combine(
//                 colorize(),
//                 timestamp(),
//                 humanReadableFormatter,
//             ),
//         })
//     ]
// });


module.exports = logger;
