var Logger = require(process.cwd() + '/lib/logger');
var Settings = require(process.cwd() + '/app/models/settings');
var Items = require(process.cwd() + '/app/models/items');
var Transactions = require(process.cwd() + '/app/models/transactions');
var Promise = require('bluebird');
var uniqBy = require('lodash/uniqBy');
var sampleSize = require('lodash/sampleSize');
var differenceWith = require('lodash/differenceWith');
var isEqual = require('lodash/isEqual');

function enqueue(currentQueue){
    let queue = Object.assign({}, currentQueue, {active: true});
    
    return updateQueue(queue);
}

function updateQueue(currentQueue){
    if(currentQueue.active == false){
        return Promise.resolve(currentQueue);
    }
    return Promise.all([getTotals(), getItems()])
        .then((response) => {
            let [totals, items] = response;
            
            let mayCall = items.reduce((acc, item) =>{
                let paidForItem = totals.find(it =>{
                    return String(it._id) == String(item._id);
                });

                if(paidForItem == undefined || paidForItem.totalPaid < item.winningBid){
                    return acc.concat(item.winners);
                }
                else{
                    return acc;
                }
            }, []);
            
            // filters out those already in the queue
            mayCall = differenceWith(mayCall, currentQueue.queue, isEqual);

            //returns the list of all people connected to unpaid items
            //they are ordered by number so this order can be used if desired
            mayCall = uniqBy(mayCall, '_id').sort((a, b) => {
                return a.number - b.number;
            });
            
            return Promise.all([
                getSettings(), 
                mayCall
            ]);
        })
        .then(response => {
            let [settings, mayCall] = response;
            let newQueue = currentQueue.queue;
            let needed = settings.queueNum - currentQueue.queue.length;
            // ensures that needed is not longer than the elements remaining
            needed = needed > mayCall.length ? mayCall.length : needed;
            
            let sample = sampleSize(mayCall, needed);
            
            // if(settings.queueOrder == 'random'){
            newQueue = newQueue.concat(sample);
            // }
            //add other order options here
            
            return {
                queue: newQueue,
                totalRemaining: mayCall.length + currentQueue.queue.length,
                active: currentQueue.active
            };
        })
        .catch(err => {
            Logger.error(err);
        });
}

function getSettings(){
    return Settings.findOne({type: 'user'}).exec();
}

function getTotals(){

    // compile transactions to get totals paid for each item
    // would love to use "lookup", but introduced in MongoDB 3.2.6 and therefore not compliant RaspPi
    // reductions etc., must be done client side

    return Transactions.aggregate()
        .project({items: 1, _id:0})
        .unwind('items')
        .group({
            _id: '$items.item',
            totalPaid: {$sum: '$items.amount'}
        });
}

function getItems(){
    // returns list of items that were won
    return Items.find({winningBid: {$exists: true}}, '_id winningBid winners').populate('winners').exec();
}

function dequeue(currentQueue, idToDequeue){
    if(currentQueue.active){
        let tempQueue = currentQueue.queue.filter(att => {
            return att._id != idToDequeue;
        });
        
        currentQueue.queue = tempQueue;
        
        return updateQueue(currentQueue);
    }
    else{
        // no need to dequeue if no queue
        return Promise.resolve(currentQueue);
    }
}

module.exports = {
    enqueue: enqueue,
    dequeue: dequeue,
    updateQueue: updateQueue
};