'use strict';

function formatPhone(number){
    number = number.replace(/\D/g, '');
    var len = number.length;
    
    var end = number.substring(len - 4);
    var prefix = '';
    var area = '';
    var intl = '';
    
    if(len > 4){
        prefix = number.substring(len - 7, len - 4) + '-';
    }
    if(len > 7){
        area = '(' + number.substring(len - 10, len -7) + ') ';
    }
    if(len > 10){
        intl = '+' + number.substring(0, len - 10) + ' ';
    }
    
    return intl + area + prefix + end;
}


module.exports = {
    formatPhone: formatPhone
};