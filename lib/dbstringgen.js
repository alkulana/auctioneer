/* eslint-disable no-console */

let process = require('process');
require('dotenv').config();


module.exports = function(){
    var dbHost = process.env.DB_HOST || 'localhost';
    var dbName = process.env.NODE_ENV == 'test' ? 'test' : process.env.DB_NAME || 'auctioneer';
    var dbPort = process.env.DB_PORT || 27017;
    var uri;

    if(/mongodb.net/.test(dbHost) && process.env.DB_USER && process.env.DB_PASS){
        let creds = `${encodeURIComponent(process.env.DB_USER)}:${encodeURIComponent(process.env.DB_PASS)}`
        uri = `mongodb+srv://${creds}@${dbHost}/?retryWrites=true&w=majority`;
    }
    else if (process.env.DB_USER && process.env.DB_PASS){
        let creds = `${process.env.DB_USER}:${process.env.DB_PASS}`
        uri = `mongodb://${creds}@${dbHost}:${dbPort}/${dbName}`;
    }
    else{
        uri = `mongodb://${dbHost}:${dbPort}/${dbName}`;
    }

    return uri;
}
