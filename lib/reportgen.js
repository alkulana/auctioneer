var Logger = require(process.cwd() + '/lib/logger');
var Transactions = require(process.cwd() + '/app/models/transactions');
var Promise = require('bluebird');
var dbFuncs = require(process.cwd() + '/lib/dbfuncs');
var spawn = require('child_process').spawn;
var moment = require('moment');
var fs = require('fs-extra');

function generateAAR(){
    fs.ensureDir(process.cwd() + '/tmp');
    return Promise.all([dbFuncs.updateData(),
        // I didn't use this?
        Transactions.aggregate()
            .project({_id: 1, items: 1, donationAmt: 1, attendee: 1})
            .unwind('items')
            .group({_id: '$attendee',
                donationAmt: {$sum: '$donationAmt'},
                items: {$addToSet: '$items'}
            })
    ])
        .then(([data, donationInfo]) =>{
            let timestamps = data.transactions.map(trans => trans.timestamp);
            let auctionDate = moment(Math.min(...timestamps)).format('MMDDYYYY');
            
            const generator = process.cwd() + '/bin/genAAR.py';
            const template = process.cwd() + '/reports/templates/AAR_template.xlsx';
            const saveName = process.cwd() + '/tmp/AAR-' + auctionDate + '.xlsx';
            
            data.itemsSold = data.items.filter(item => item.winningBid !== null).length;
            data.donationOnly = data.transactions.filter(trans => trans.hasOwnProperty('donationAmt') && trans.donationAmt !== null && trans.donationAmt > 0 && trans.items.length === 0);
            
            let newData = JSON.stringify(data);
 
            return new Promise((resolve, reject) => {
                let stdOut = '';
                
                let py = spawn('pipenv', ['run', 'python3', generator, template, saveName]);
                
                py.stdout.on('data', data => {
                    stdOut += data;
                });
                py.stdout.on('end', () => {
                    resolve(stdOut);
                });
                
                py.stderr.on('data', data => {
                    Logger.info(data.toString());
                });
                
                py.stdin.write(newData);
                py.stdin.write('\n');
                py.stdin.write(moment(Math.min(...timestamps)).format('MM/DD/YYYY'));
                py.stdin.end();
                py.on('error', reject);
            });
        })
        .then((stdOut) => {
            
            
            return Promise.resolve(stdOut.trim());
        })
        .catch(err => {
            Logger.error(err);
        });
}



module.exports = {
    generateAAR,
    
};