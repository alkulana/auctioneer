/* eslint-disable no-console */

var mongoose = require('mongoose');
var Promise = require('bluebird');
mongoose.Promise = Promise;
var Settings = require('../app/models/settings');
let process = require('process');
require('dotenv').config();
var dbStringGen = require('./dbstringgen');


module.exports = function(){
    var uri = dbStringGen();
    var dbName = process.env.NODE_ENV == 'test' ? 'test' : process.env.DB_NAME || 'auctioneer';

    const defaultSettings = {
        type: 'default',
        useNum: true,
        startNum: 100,
        queueNum: 3,
        advanceByPaid: true,
        advanceByClick: false,
        queueDisplay: 'number',
        queueOrder: 'random'
    };

    const setupQuery = Settings.findOneAndUpdate(
        {type: 'default'},
        defaultSettings,
        {upsert: true, new: true}
    );

    return mongoose.connect(uri,
         {
             family: 4,
             dbName: dbName
    //     useFindAndModify: false,
    //     useCreateIndex: true
    }
    ).then(() => {
        return setupQuery.exec()
            .then(res => {
                return console.log(res);
            })},
        err => console.log(err)
    )
};

if(require.main === module){
    module.exports()
        .then(() => {
            process.exit(0);
        })
        .catch(e => {
            console.log(e);
            process.exit(1);
        });
}
