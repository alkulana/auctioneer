var Logger = require(process.cwd() + '/lib/logger');
var Settings = require(process.cwd() + '/app/models/settings');
var Attendees = require(process.cwd() + '/app/models/attendees');
var Items = require(process.cwd() + '/app/models/items');
var Transactions = require(process.cwd() + '/app/models/transactions');
var Promise = require('bluebird');
var ObjectId = require('mongoose').Types.ObjectId;
var Big = require('big.js');
var _ = require('lodash');



function getSettings(){
    let defaultSettings = Settings.schema.obj;
    return Settings.find({type: 'default'}).exec()
        .then(results => {
            //ensures that default is created and that the keys are all inexistence
            let nextStep;
            if(!results.length || results.length === 0){
                nextStep = Settings.create({type: 'default'});
            }
            else{
                let update = {};
                for(const [key, subObj] of Object.entries(defaultSettings)){
                    if(!results[0].toJSON().hasOwnProperty(key)){
                        update[key] = subObj.default;
                    }
                }
                nextStep = Object.keys(update).length > 0 ? Settings.updateOne({type: 'default'}, update).exec() : Promise.resolve();
            }
            return nextStep;
        })
        .then(() => {
            // checks for user settings and creates as needed
            return Settings.find({type: 'user'}, {__v:0}).exec();
        })
        .then(results => {
            let userSettings;
            if(!results.length){

                userSettings = Settings.create({type: 'user'});
            }
            else{
                let update = {};
                for(const [key, subObj] of Object.entries(defaultSettings)){
                    if(!results[0].toJSON().hasOwnProperty(key)){
                        update[key] = subObj.default;
                    }
                }
                userSettings = Object.keys(update).length > 0 ? Settings.updateOne({type: 'user'}, update, {new: true, fields: {__v: 0}}).exec() : Promise.resolve(results[0]);
            }
            return userSettings;
        })
        .catch(err =>{
            Logger.error(err);
        });
}

function updateSettings(settings){
    delete settings.__v;
    return Settings.findOneAndUpdate({_id: settings._id}, settings, {new: true});
}


function updateData(){
    return Promise.all([
        Attendees.find({}, {__v: 0}).sort('number').exec(),
        Items.find({}, {__v: 0}).populate('winners').sort('itemNumber').exec(),
        Transactions.find({}, {__v: 0}).populate('attendee').populate('items.item').sort('-timestamp').exec(),
        parseTotals(),
        purchaseStatus()
    ])
        .then(([attendees, items, transactions, totals, status]) => {
            items = items.map(item => {
                return item.toObject({virtuals: true});
            });

            transactions = transactions.map(trans => {
                return trans.toObject({virtuals: true});
            });

            return {
                attendees: attendees,
                items: items,
                transactions: transactions,
                totals: totals,
                status: status
            };
        })
        .then(obj =>{
            let summaries = createSummaries(obj);

            return Promise.resolve(Object.assign(obj, {summaries: summaries}));
        });
}


/**
 * Add data of all types except settings to the database
 * @param {Object} data - The object that contains all of the information regarding the data
 * @param {string[]} data.types - list of the types of data included
 * @param {boolean} [data.clean[ - whether or not to empty the tables listed in data.types before adding data
 * @param {Object[]} [data.attendees] - array of attendees to be added
 * @param {Object[]} [data.items] - array of itmes to be added
 * @param {Object[]} [data.winners] - array of winners to be added
 * @param {Object[]} [data.transactions] - array of transactions to be added
 */
function addData(data){
    var funcList = [];

    if(data.clean && data.clean === true){
        funcList.push(dataClean);
    }
    if(data.types.includes('attendees')){
        funcList.push(addAttendees);
    }
    if(data.types.includes('items')){
        funcList.push(addItems);
    }
    if(data.types.includes('winners')){
        funcList.push(addWinners);
    }
    if(data.types.includes('transactions')){
        funcList.push(addTransactions);
    }

    return Promise.each(funcList, func => func(data))
        .then(() => {
            return updateData();
        });
}


//this can probably be cleaned up
function dataClean(data){
    var types = data.types.filter(type => type !== 'winners');

    var typeList = {
        attendees: Attendees,
        items: Items,
        transactions: Transactions
    };

    var transactions;

    if(data.types.includes('transactions') && !data.types.includes('items') && !data.types.includes('winners')){
        transactions = Items.find({'winners.0': {$exists: true}}).exec()
            .then(t =>{
            return t.map(item => {
                item.winners.forEach(winner => {
                    winner.paid = null;
                    winner.amountPaid = null;
                    winner.transID = null;
                });
                return item.save();
            })})
    }
    else{
        transactions = Promise.resolve();
    }

    var winners = data.types.includes('winners') && !data.types.includes('items') ? Items.updateMany({}, {$set: {winners: [], winningBid: null}}).exec(): Promise.resolve();

    return transactions
        .then(() => {
            return winners;
        })
        .then(() => {
            return Promise.map(types, type => {
                typeList[type].deleteMany({}).exec();
            });
        })
        .then(() => {
            return updateData();
        });
}

function addAttendees(data){
    var {attendees} = data;
    if(attendees.length > 1){
        return Attendees.insertMany(attendees);
    }
    else if(attendees.hasOwnProperty('_id')){
        return Attendees.updateOne({_id: attendees._id}, attendees).exec();
    }
    else{
        return Attendees.create(attendees);
    }
}

function addItems(data){
    var {items} = data;
    if(items.length > 1){
        return Items.insertMany(items);
    }
    else if(items.hasOwnProperty('_id')){
        return Items.updateOne({_id: items._id}, items).exec();
    }
    else{
        return Items.create(items);
    }
}

//does not check to ensure that there are attendees or items
//only for dummy data
function addWinners(data){
    if(data.dummy === true){
        var items;
        return Items.find().exec()
            .then(i => {
                items = i;
                return Attendees.find().exec();
            })
            .then(att => {
                var attendees = [];
                for(var i = 0; i < items.length; i++){
                    attendees.push(att[Math.floor(Math.random() * att.length)]._id.toString());
                }
                var bulk = items.map((item, idx) => {
                    return {
                        updateOne: {
                            filter: {_id: item._id},
                            update: {
                                winningBid: +((Math.random() * 300).toFixed(2)),
                                $push: {winners: new ObjectId(attendees[idx])}
                            }
                        }
                    };
                });
                return Items.bulkWrite(bulk);
            });
    }
    else{
        //complete when working with actual data
        return 0;
    }
}

function addTransactions(data){
    if(!data.dummy === true){
        let {attendee, items, donation, paymentType} = data;

        items = items.map(item => {
            return {
                item: item._id,
                amount: item.amtPaying
            };
        });

        return Transactions.create({
            attendee: attendee,
            items: items,
            donationAmt: donation,
            paymentType: paymentType
        })
            .then(() => {
                return updateData();
            })
            .catch(err =>{
                Logger.error(err);
            });
    }
    // below for adding test data
    else{

        return Items.aggregate()
            .unwind('winners')
            .group({
                _id: '$winners',
                items: {$push: {item: '$_id',
                    amount: '$winningBid'}
                }
            })
            .then(aggregate => {
                var bulk = aggregate.map(transaction => {
                    return {
                        insertOne:{
                            document:{
                                attendee: new ObjectId(transaction._id),
                                timestamp: Date.now(),
                                items: transaction.items,
                                donationAmt: +((Math.random() * 200).toFixed(2)),
                                paymentType: ['cash', 'check', 'card'][Math.floor(Math.random()*3)]
                            }
                        }
                    };
                });
                return Transactions.bulkWrite(bulk);
            });
    }
}

function deleteData(data){
    var typeList = {
        attendees: Attendees,
        items: Items,
        transactions: Transactions
    };

    return typeList[data.type].deleteOne({_id: data.id}).exec()
        .then(() => {
            return updateData();
        });
}


// data format = {[id]: {winners: [], winningBid: number}, etc}
function bulkAddWinners(data){
    var keys = Object.keys(data);
    var list = [];
    keys.forEach(v => {
        if(data[v].winningBid){
            //updates winningBid
            list.push({
                updateOne: {
                    filter: {_id: v},
                    update: {
                        // winners: data[v].winners,
                        winningBid: data[v].winningBid
                    },
                    upsert: true
                }
            });
        }

        if(data[v].winners){
            //removes winners not in list
            var wList = data[v].winners.map(w => new ObjectId(w));
            list.push({
                updateOne: {
                    filter: {_id: v},
                    update: {
                        $pull: {winners: {$nin: wList}}
                    }
                }
            });

            //adds winners that are in the list, ignoring those already there
            list.push({
                updateOne: {
                    filter: {_id: v},
                    update: {
                        $addToSet: {winners: {$each: wList}}
                    },
                    upsert: true
                }
            });
        }
    });

    return Items.bulkWrite(list)
        .then(() => {
            return updateData();
        });
}


function updateSingleWinner(data){
    var id = Object.keys(data)[0];

    var bid = Promise.resolve();
    var winnersRemove = Promise.resolve();
    var winnersAdd = Promise.resolve();

    if(data[id].winningBid){
        bid = Items.updateOne(
            {_id: id},
            {winningBid: data[id].winningBid},
            {upsert: true}
        ).exec();
    }

    if(data[id].winners){
        var wList = data[id].winners.map(w => new ObjectId(w));
        winnersRemove = Items.updateOne(
            {_id: id},
            {$pull: {winners: {$nin: wList}}}
        ).exec();

        winnersAdd = Items.updateOne(
            {_id: id},
            {$addToSet: {winners: {$each: wList}}},
            {upsert: true}
        ).exec();
    }

    return bid
        .then(() => {
            return winnersRemove;
        })
        .then(() => {
            return winnersAdd;
        })
        .then(() => {
            return updateData();
        });
}

function splitCost(data){
    var {itemID, attList} = data;

    // variable never used.  What was I doing?
    return Attendees.find({_id: {$in: attList}})
        .exec()
        .then(winnerList => {
            return Items.findByIdAndUpdate(
                itemID,
                {
                    winners: attList
                }
            ).exec();
        })
        .then(() => updateData());
}

function deleteTransaction(data){
    var {transID} = data;

    return Transactions.deleteOne({_id: transID})
        .exec()
        .then(() => updateData());
}

function editTransaction(data){
    var {transID, items, donation, paymentType} = data;

    items = items.map(item => {
        return {
            item: item._id,
            amount: item.amtPaying
        };
    });

    return Transactions.findOneAndUpdate(
        {_id: transID},
        {
            items: items,
            donationAmt: donation,
            paymentType: paymentType
        }
    ).exec()
        .then(() => {
            return updateData();
        })
        .catch(err =>{
            Logger.error(err);
        });
}

function parseTotals(){
    let byItem = Transactions.aggregate()
        .project({paymentType: 1, items: 1})
        .unwind('items')
        .group({_id: '$paymentType', total: {$sum: '$items.amount'}});

    let byDonation = Transactions.aggregate()
        .project({paymentType: 1, donationAmt: 1})
        .group({_id: '$paymentType', total: {$sum: '$donationAmt'}});

    return Promise.all([byItem, byDonation])
        .then(([byItem, byDonation])=> {
            let keys = ['cash', 'card', 'check'];

            byItem = byItem.reduce((acc, item) => {
                acc[item._id] = Big(item.total).round(2);
                return acc;
            }, {});

            byDonation = byDonation.reduce((acc, item) => {
                acc[item._id] = Big(item.total).round(2);
                return acc;
            }, {});

            //There's probably a way to do this in the aggregation stage
            keys.forEach(key => {
                if(!Object.keys(byItem).includes(key)){
                    byItem[key] = Big(0);
                }
            });
            keys.forEach(key => {
                if(!Object.keys(byDonation).includes(key)){
                    byDonation[key] = Big(0);
                }
            });


            byItem.itemTotal = byItem.cash.plus(byItem.card).plus(byItem.check).round(2);
            byDonation.donationTotal = byDonation.cash.plus(byDonation.card).plus(byDonation.check).round(2);

            let cashTotal = byItem.cash.plus(byDonation.cash).round(2);
            let checkTotal = byItem.check.plus(byDonation.check).round(2);
            let cardTotal = byItem.card.plus(byDonation.card).round(2);
            let cardAfterFeeTotal = cardTotal.times(.9725).round(2);

            let grandTotal = cashTotal.plus(checkTotal).plus(cardTotal).round(2);
            let afterFeeTotal = cashTotal.plus(checkTotal).plus(cardAfterFeeTotal).round(2);

            let retObj = {
                byItem,
                byDonation,
                cashTotal,
                cardTotal,
                checkTotal,
                cardAfterFeeTotal,
                grandTotal,
                afterFeeTotal
            };

            return retObj;
        });

}

//first determine what items need to be paid for still, then determine who needs to pay
function purchaseStatus(){
    return Promise.all([
        Items.find({}, {_id: 1, winningBid: 1, winners: 1}).lean().exec(),
        Transactions.aggregate()
            .project({items: 1, _id:0})
            .unwind('items')
            .group({
                _id: '$items.item',
                totalPaid: {$sum: '$items.amount'}
            }),
        Items.aggregate()
            .project({_id: 1, winners: 1, itemName: 1, itemNumber: 1})
            .unwind('winners')
            .group({_id: '$winners', items: {$addToSet: {_id: '$_id', itemName: '$itemName', itemNumber: '$itemNumber'}}})
    ])
        .then(([items, itemTotals, winnersWithItems]) =>{
            let statusByItem = items.reduce((acc, item) =>{
                let total = itemTotals.find(it => {
                    return String(item._id) == String(it._id);
                });
                if(total == undefined){
                    let id = String(item._id);
                    delete item._id;
                    let newObj = Object.assign({}, {_id: id}, item);
                    acc.unpaid.push(newObj);
                    return acc;
                }
                else if(total.totalPaid < item.winningBid){
                    let id = String(item._id);
                    delete item._id;
                    delete total._id;
                    let newObj = Object.assign({}, {_id: id}, item, total);
                    acc.partiallyPaid.push(newObj);
                    return acc;
                }
                else{
                    let id = String(item._id);
                    delete item._id;
                    delete total._id;
                    let newObj = Object.assign({}, {_id: id}, item, total);
                    acc.fullyPaid.push(newObj);
                    return acc;
                }
            }, {fullyPaid: [], partiallyPaid: [], unpaid: []});

            let statusByItemArray = _.transform(statusByItem, ((acc, value, key) => {
                value.forEach(val => {
                    acc.push({status: key, ...val});
                });
            }), []);

            let statusByWinner = winnersWithItems.reduce((acc, winner) => {
                let matrix = {
                    unpaid: 0,
                    partiallyPaid: 0.5,
                    fullyPaid: 1
                };

                // if paid enter 1, if partial enter .5, if not enter 0, but kill process if one of last two
                let rating = [];

                winner.items.forEach(item => {
                    let ret = statusByItemArray.find(it => {
                        return String(it._id) == String(item._id);
                    });
                    if(ret == undefined){
                        rating.push(0);
                    }
                    else{
                        rating.push(matrix[ret.status]);
                    }
                });
                let finalRating = rating.reduce((a,b)=>a+b) / rating.length;

                if(finalRating === 0){
                    acc.unpaid.push(winner._id);
                    return acc;
                }
                else if(finalRating === 1){
                    acc.fullyPaid.push(winner._id);
                    return acc;
                }
                else{
                    acc.partiallyPaid.push(winner._id);
                    return acc;
                }
            }, {fullyPaid: [], partiallyPaid: [], unpaid: []});

            return {statusByItem, statusByWinner, winnersWithItems};
        });

}


function createSummaries(dataSet){
    let { attendees, transactions, status } = dataSet;
    let { statusByWinner, winnersWithItems } = status;
    let summaryObj = [];

    Object.keys(statusByWinner).forEach(status => {
        statusByWinner[status].forEach(winner => {
            let baseObj = {
                status: status,
                _id: winner
            };

            let itemsWon = winnersWithItems.find(win => {
                return win._id == winner;
            }).items;
            // console.log(winner)
            // console.log(itemsWon)

            let winnerTransactions = transactions.filter(trans => {
                return trans.hasOwnProperty('attendee') && String(trans.attendee._id) == winner;
            });
            // console.log(winnerTransactions)

            let totals = winnerTransactions.reduce((acc, trans) => {
                if(trans.hasOwnProperty('donationAmt') && trans.donationAmt !== null){
                    acc.donationAmt = acc.hasOwnProperty('donationAmt') ? Big(trans.donationAmt).plus(acc.donationAmt) : Big(trans.donationAmt);
                }

                trans.items.forEach(transItem => {
                    acc[String(transItem.item._id)] = acc.hasOwnProperty(String(transItem.item._id)) ? Big(transItem.amount).plus(acc[String(transItem._id)]) : Big(transItem.amount);
                });

                return acc;
            }, {});

            totals.grandTotal = Object.keys(totals).reduce((acc, key) => {
                return Big(totals[key]).plus(acc);
            }, Big(0));

            Object.keys(totals).forEach(key => {
                totals[key] = totals[key].toFixed(2);
            });

            // console.log(totals)

            itemsWon.forEach((item, idx) => {
                let itemTotal = totals.hasOwnProperty(String(item._id)) ? totals[String(item._id)] : '0.00';
                itemsWon[idx]['itemTotal'] = itemTotal;
            });

            // console.log(itemsWon)
            let attendee;
            if(winnerTransactions.length > 0){
                attendee = winnerTransactions[0].attendee;
            }
            else{
                //in case they hadn't purchased anything yet
                let tmpAttendee = attendees.find(att => {
                    return String(att._id) == String(winner);
                });

                if (typeof tmpAttendee == 'undefined'){
                    return
                }

                attendee = {
                    number: tmpAttendee.number,
                    name: tmpAttendee.name,
                    phone: tmpAttendee.phone,
                    email: tmpAttendee.email
                };
            }

            let returnObj = Object.assign(baseObj, attendee, {itemsWon: itemsWon, donationAmt: totals.donationAmt || '0.00', grandTotal: totals.grandTotal} );

            summaryObj.push(returnObj);
        });
    });
    return summaryObj;
}

function handleUpload(data){
    // let processedData = processUpload(data)
    // should send {types: ['attendee'], attendees: []} or {types: ['item'], items: []} to the addData function above

    return processUpload(data).then(processedData =>{
        return addData(processedData)
    })
}

function processUpload(data){
    // gets the csv data into the right format
    // this does not yet handle collisions for the item/attendee numbers
    let {csvHeadings, csvData, uploadType, selectValues, combineName, combinedIdx} = data;

    return getHighest(uploadType).then(ctr =>{
        ctr += 1;
        let formatted = csvData.map(entry =>{
            let processed = {};
            let selectKeys = Object.keys(selectValues);
            for (let i = 0; i < selectKeys.length; i++){
                if(combineName && selectKeys[i] == "Name"){
                    let combinedName = entry[csvHeadings[combinedIdx[0]]] + " " + entry[csvHeadings[combinedIdx[1]]];
                    processed[selectKeys[i].toLowerCase()] = combinedName;
                }
                else if(uploadType == 'item' && selectKeys[i] == 'Name'){
                    processed['itemName'] = entry[selectValues[selectKeys[i]]];
                }
                else{
                    processed[selectKeys[i].toLowerCase()] = entry[selectValues[selectKeys[i]]];
                }

                // ctr = ctr + 1;
                // sets how numbers are handled
                if(selectKeys[i] == 'Number' && selectValues[selectKeys[i]] == 'Generate'){
                    if(uploadType == 'item'){
                        processed['itemNumber'] = ctr;
                    }
                    else if(uploadType == 'attendee'){
                        processed['number'] = ctr;
                    }
                }
                else if(selectKeys[i] == 'Number'){
                    var tempNum;
                    if(isNaN(Number(entry[selectValues[selectKeys[i]]]))){
                        tempNum = ctr;
                    }
                    else{
                        tempNum = Number(entry[selectValues[selectKeys[i]]]);
                    }
                    // const tempNum = isNaN(Number(entry[selectValues[selectKeys[i]]])) == 'number' ? ctr : entry[selectValues[selectKeys[i]]];
                    if(uploadType == 'item'){
                        processed['itemNumber'] = tempNum;
                    }
                    else if(uploadType == 'attendee'){
                        processed['number'] = tempNum;
                    }
                    ctr = tempNum;
                }
            }
            ctr += 1;

            return processed;
        })
        let finalFormat = {};
        if(data.uploadType=="item"){
            finalFormat = {
                types: ['items'],
                items: formatted
            };
        }
        else if(data.uploadType == "attendee"){
            finalFormat = {
                types: ['attendees'],
                attendees: formatted
            };
        }
        return finalFormat;
    })
}

function getHighest(uploadType){
    let max;
    if (uploadType == "item"){
        max = Items.distinct('itemNumber');
    }
    else if(uploadType == "attendee"){
        max = Attendees.distinct('number');
    }
    return max.then(result => {
        if(result.length == 0){
            return 0
        }
        else{
            return Math.max(...result)
        }
    })
    .catch(err => {
        Logger.error(err);
    })
}

module.exports = {
    getSettings: getSettings,
    addData: addData,
    updateData: updateData,
    dataClean: dataClean,
    deleteData: deleteData,
    bulkAddWinners: bulkAddWinners,
    updateSingleWinner: updateSingleWinner,
    splitCost: splitCost,
    addTransactions: addTransactions,
    deleteTransaction: deleteTransaction,
    editTransaction: editTransaction,
    updateSettings: updateSettings,
    handleUpload: handleUpload
};
